use std::fmt::Display;
use itertools::Itertools;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::newline;
use nom::combinator::map;
use nom::IResult;
use nom::multi::many1;
use crate::day_14::Thing::{EmptySpace, KubeShapedRock, RoundedRock};

#[derive(Clone, PartialEq, Debug)]
enum Thing {
    RoundedRock,
    KubeShapedRock,
    EmptySpace,
}

#[derive(PartialEq, Debug)]
struct Platform {
    positions: Vec<Vec<Thing>>,
}

impl Platform {
    fn roll_north(&self) -> Platform {
        let mut new_positions = self.positions.clone();

        for y in 1..new_positions.len() {
            for x in 0..new_positions[0].len() {
                if new_positions[y][x] == RoundedRock {
                    let new_y = find_roll_north_destination(&new_positions, x, y);
                    new_y.iter().for_each(|&destination| {
                        new_positions[destination][x] = RoundedRock;
                        new_positions[y][x] = EmptySpace;
                    })
                }
            }
        }

        Platform { positions: new_positions }
    }

    fn roll_west(&self) -> Platform {
        let mut new_positions = self.positions.clone();

        for x in 1..new_positions[0].len() {
            for y in 0..new_positions.len() {
                if new_positions[y][x] == RoundedRock {
                    let new_x = find_roll_west_destination(&new_positions, x, y);
                    //println!("New x {new_x:?}");
                    new_x.iter().for_each(|&destination| {
                        new_positions[y][destination] = RoundedRock;
                        new_positions[y][x] = EmptySpace;
                    })
                }
            }
        }

        Platform { positions: new_positions }
    }

    fn roll_south(&self) -> Platform {
        let mut new_positions = self.positions.clone();

        for y in (0..new_positions.len() - 1).rev() {
            for x in 0..new_positions[0].len() {
                if new_positions[y][x] == RoundedRock {
                    let new_y = find_roll_south_destination(&new_positions, x, y);
                    new_y.iter().for_each(|&destination| {
                        new_positions[destination][x] = RoundedRock;
                        new_positions[y][x] = EmptySpace;
                    })
                }
            }
        }

        Platform { positions: new_positions }
    }

    fn roll_east(&self) -> Platform {
        let mut new_positions = self.positions.clone();

        for x in (0..new_positions[0].len() - 1).rev() {
            for y in 0..new_positions.len() {
                if new_positions[y][x] == RoundedRock {
                    let new_x = find_roll_east_destination(&new_positions, x, y);
                    new_x.iter().for_each(|&destination| {
                        new_positions[y][destination] = RoundedRock;
                        new_positions[y][x] = EmptySpace;
                    })
                }
            }
        }

        Platform { positions: new_positions }
    }

    fn calculate_load_on_north_support_beam(&self) -> usize {
        let max_row_weight = self.positions.len();
        self.positions.iter().enumerate().map(|(y, row)| {
            let row_weight = max_row_weight - y;
            row_weight * row.iter().filter(|&thing| thing == &RoundedRock).count()
        }).sum()
    }

    fn cycle(&self) -> Platform {
        self.roll_north().roll_west().roll_south().roll_east()
    }
}

impl Display for Platform {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = self.positions.iter().map(|line| {
            line.iter().map(|thing| {
                match thing {
                    RoundedRock => "O",
                    KubeShapedRock => "#",
                    EmptySpace => ".",
                }
            }).join("")
        }).join("\n");
        write!(f, "{}", str)
    }
}

fn find_roll_north_destination(positions: &Vec<Vec<Thing>>, x: usize, current_y: usize) -> Option<usize> {
    let mut result = None;
    for y in (0..current_y).rev() {
        if positions[y][x] == EmptySpace {
            result = Some(y);
        } else {
            break;
        }
    }
    result
}

fn find_roll_west_destination(positions: &Vec<Vec<Thing>>, current_x: usize, y: usize) -> Option<usize> {
    let mut result = None;
    for x in (0..current_x).rev() {
        if positions[y][x] == EmptySpace {
            result = Some(x);
        } else {
            break;
        }
    }
    result
}

fn find_roll_south_destination(positions: &Vec<Vec<Thing>>, x: usize, current_y: usize) -> Option<usize> {
    let mut result = None;
    for y in current_y + 1..positions[current_y].len() {
        if positions[y][x] == EmptySpace {
            result = Some(y);
        } else {
            break;
        }
    }
    result
}

fn find_roll_east_destination(positions: &Vec<Vec<Thing>>, current_x: usize, y: usize) -> Option<usize> {
    let mut result = None;
    for x in current_x + 1..positions.len() {
        if positions[y][x] == EmptySpace {
            result = Some(x);
        } else {
            break;
        }
    }
    result
}

fn thing(input: &str) -> IResult<&str, Thing> {
    let (input, thing) = alt((
        map(tag("O"), |_| { RoundedRock }),
        map(tag("#"), |_| { KubeShapedRock }),
        map(tag("."), |_| { EmptySpace }),
    ))(input)?;
    Ok((input, thing))
}

fn row(input: &str) -> IResult<&str, Vec<Thing>> {
    let (input, row) = many1(thing)(input)?;
    let (input, _) = newline(input)?;

    Ok((input, row))
}

fn platform(input: &str) -> IResult<&str, Platform> {
    let (input, positions) = many1(row)(input)?;

    Ok((input, Platform { positions }))
}

fn solve_part_1(input: &str) -> usize {
    let (_, platform) = platform(input).unwrap();
    platform.roll_north().calculate_load_on_north_support_beam()
}

fn solve_part_2(input: &str) -> usize {
    let (_, platform) = platform(input).unwrap();
    let mut current_platform = platform;
    for _ in 0..1_000_000_000 {
        current_platform = current_platform.cycle();
    }
    current_platform.calculate_load_on_north_support_beam()
}

#[cfg(test)]
mod tests {
    use crate::day_14::{Platform, solve_part_1, solve_part_2};
    use crate::day_14::Thing::{EmptySpace, KubeShapedRock, RoundedRock};
    use crate::read_file;

    #[test]
    fn should_roll_all_rounded_rocks_north() {
        let input = Platform {
            positions: vec![
                // O....#....
                vec![RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // O.OO#....#
                vec![RoundedRock, EmptySpace, RoundedRock, RoundedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock],
                // .....##...
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace],
                // OO.#O....O
                vec![RoundedRock, RoundedRock, EmptySpace, KubeShapedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock],
                // .O.....O#.
                vec![EmptySpace, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, EmptySpace],
                // O.#..O.#.#
                vec![RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ..O..#O..O
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, RoundedRock, EmptySpace, EmptySpace, RoundedRock],
                // .......O..
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace],
                // #....###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #OO..#....
                vec![KubeShapedRock, RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };

        let result = input.roll_north();

        let expected = Platform {
            positions: vec![
                // OOOO.#.O..
                vec![RoundedRock, RoundedRock, RoundedRock, RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, EmptySpace, EmptySpace],
                // OO..#....#
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock],
                // OO..O##..O
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace, RoundedRock],
                // O..#.OO...
                vec![RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace],
                // ........#.
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace],
                // ..#....#.#
                vec![EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ..O..#.O.O
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, EmptySpace, RoundedRock],
                // ..O.......
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // #....###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #....#....
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };
        assert_eq!(expected, result);
    }

    #[test]
    fn should_calculate_load_on_north_support_beam() {
        let input = Platform {
            positions: vec![
                // OOOO.#.O..
                vec![RoundedRock, RoundedRock, RoundedRock, RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, EmptySpace, EmptySpace],
                // OO..#....#
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock],
                // OO..O##..O
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace, RoundedRock],
                // O..#.OO...
                vec![RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace],
                // ........#.
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace],
                // ..#....#.#
                vec![EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ..O..#.O.O
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, EmptySpace, RoundedRock],
                // ..O.......
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // #....###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #....#....
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };

        assert_eq!(input.calculate_load_on_north_support_beam(), 136);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_14/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 136);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_14/input.txt");
        assert_eq!(solve_part_1(&input), 106186);
    }

    #[test]
    #[ignore] // Doesn't complete
    fn should_solve_part_2_example() {
        let input = read_file("day_14/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 64);
    }

    #[test]
    fn should_cycle() {
        let input = Platform {
            positions: vec![
                // O....#....
                vec![RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // O.OO#....#
                vec![RoundedRock, EmptySpace, RoundedRock, RoundedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock],
                // .....##...
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace],
                // OO.#O....O
                vec![RoundedRock, RoundedRock, EmptySpace, KubeShapedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock],
                // .O.....O#.
                vec![EmptySpace, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, EmptySpace],
                // O.#..O.#.#
                vec![RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ..O..#O..O
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, RoundedRock, EmptySpace, EmptySpace, RoundedRock],
                // .......O..
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace],
                // #....###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #OO..#....
                vec![KubeShapedRock, RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };

        let result = input.cycle();

        let expected = Platform {
            positions: vec![
                //.....#....
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // ....#...O#
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock],
                // ...OO##...
                vec![EmptySpace, EmptySpace, EmptySpace, RoundedRock, RoundedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace],
                // .OO#......
                vec![EmptySpace, RoundedRock, RoundedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // .....OOO#.
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, RoundedRock, RoundedRock, KubeShapedRock, EmptySpace],
                // .O#...O#.#
                vec![EmptySpace, RoundedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ....O#....
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // ......OOOO
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, RoundedRock, RoundedRock, RoundedRock, RoundedRock],
                // #...O###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #..OO#....
                vec![KubeShapedRock, EmptySpace, EmptySpace, RoundedRock, RoundedRock, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };

        assert_eq!(result, expected);
    }

    #[test]
    fn should_roll_west() {
        let input = Platform {
            positions: vec![
                // OOOO.#.O..
                vec![RoundedRock, RoundedRock, RoundedRock, RoundedRock, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, EmptySpace, EmptySpace],
                // OO..#....#
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock],
                // OO..O##..O
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, RoundedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace, RoundedRock],
                // O..#.OO...
                vec![RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace],
                // ........#.
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace],
                // ..#....#.#
                vec![EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ..O..#.O.O
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, RoundedRock, EmptySpace, RoundedRock],
                // ..O.......
                vec![EmptySpace, EmptySpace, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // #....###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #....#....
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };

        let result = input.roll_west();

        let expected = Platform {
            positions: vec![
                // OOOO.#O...
                vec![RoundedRock, RoundedRock, RoundedRock, RoundedRock, EmptySpace, KubeShapedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace],
                // OO..#....#
                vec![RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock],
                // OOO..##O..
                vec![RoundedRock, RoundedRock, RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, RoundedRock, EmptySpace, EmptySpace],
                // O..#OO....
                vec![RoundedRock, EmptySpace, EmptySpace, KubeShapedRock, RoundedRock, RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // ........#.
                vec![EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace],
                // ..#....#.#
                vec![EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, KubeShapedRock],
                // ..O..#OO..
                vec![RoundedRock, EmptySpace, EmptySpace,  EmptySpace, EmptySpace, KubeShapedRock, RoundedRock, RoundedRock, EmptySpace, EmptySpace],
                // ..O.......
                vec![RoundedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
                // #....###..
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, KubeShapedRock, KubeShapedRock, EmptySpace, EmptySpace],
                // #....#....
                vec![KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace, KubeShapedRock, EmptySpace, EmptySpace, EmptySpace, EmptySpace],
            ]
        };

        //println!("{input}\n\n{expected}\n\n{result}\n\n");

        assert_eq!(expected, result);
    }
}