mod day_2;
mod day_3;
mod day_4;
mod day_5;
mod day_6;
mod day_7;
mod day_8;
mod day_9;
mod day_10;
mod day_11;
mod day_12;
mod day_13;
mod day_14;
mod day_15;
mod day_16;
mod day_17;
mod day_18;

use std::{env, fs};
use std::collections::HashMap;

use lazy_static::lazy_static;

pub fn read_file(path: &str) -> String {
    let cwd = env::current_dir().unwrap();
    let filepath = cwd.join("data").join(path);
    let f = fs::read_to_string(filepath);
    f.expect("could not open input file")
}

struct NumberWithLocation {
    location: usize,
    number: i32,
}

lazy_static! {
    static ref NUMBERS: HashMap<&'static str, i32> = [
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
].iter().cloned().collect();
    }

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use crate::{NUMBERS, NumberWithLocation, read_file};

    #[test]
    fn finds_first_digit() {
        let sample = "1sdf2";
        assert_eq!(sample.find(char::is_numeric).unwrap(), 0);
    }

    #[test]
    fn finds_last_digit() {
        let sample = "1sdf2";
        assert_eq!(sample.rfind(char::is_numeric).unwrap(), 4);
    }

    #[rstest]
    #[case("1abc2", 1)]
    #[case("pqr3stu8vwx", 3)]
    #[case("a1b2c3d4e5f", 1)]
    #[case("treb7uchet", 7)]
    #[case("two1nine", 2)]
    #[case("eightwothree", 8)]
    #[case("abcone2threexyz", 1)]
    #[case("xtwone3four", 2)]
    #[case("4nineeightseven2", 4)]
    #[case("zoneight234", 1)]
    #[case("7pqrstsixteen", 7)]
    fn extract_first_number_char(#[case] sample: &str, #[case] expected: i32) {
        let first_digit_char = get_first_digit(sample);
        assert_eq!(first_digit_char, expected);
    }

    fn get_first_digit(sample: &str) -> i32 {
        let first_digit_location = sample.find(char::is_numeric).map(|index| {
            NumberWithLocation {
                location: index,
                number: sample.chars().nth(index).unwrap().to_digit(10).unwrap() as i32,
            }
        });
        let mut number_positions: Vec<Option<NumberWithLocation>> = NUMBERS.iter().map(|t| {
            sample.find(t.0).map(|i| {
                NumberWithLocation { location: i, number: *t.1 }
            })
        }).collect();
        number_positions.push(first_digit_location);
        let result = number_positions.iter().flatten().into_iter().min_by_key(|num_with_location| { num_with_location.location }).unwrap().number;
        result
    }

    fn get_last_digit(sample: &str) -> i32 {
        let first_digit_location = sample.rfind(char::is_numeric).map(|index| {
            NumberWithLocation {
                location: index,
                number: sample.chars().nth(index).unwrap().to_digit(10).unwrap() as i32,
            }
        });
        let mut number_positions: Vec<Option<NumberWithLocation>> = NUMBERS.iter().map(|t| {
            sample.rfind(t.0).map(|i| {
                NumberWithLocation { location: i, number: *t.1 }
            })
        }).collect();
        number_positions.push(first_digit_location);
        let result = number_positions.iter().flatten().into_iter().max_by_key(|num_with_location| { num_with_location.location }).unwrap().number;
        result
    }

    #[rstest]
    #[case("1abc2", 2)]
    #[case("pqr3stu8vwx", 8)]
    #[case("a1b2c3d4e5f", 5)]
    #[case("treb7uchet", 7)]
    #[case("two1nine", 9)]
    #[case("eightwothree", 3)]
    #[case("abcone2threexyz", 3)]
    #[case("xtwone3four", 4)]
    #[case("4nineeightseven2", 2)]
    #[case("zoneight234", 4)]
    #[case("7pqrstsixteen", 6)]
    fn extract_last_number_char(#[case] sample: &str, #[case] expected: i32) {
        let last_digit = get_last_digit(sample);
        assert_eq!(last_digit, expected);
    }

    #[rstest]
    #[case("1abc2", 12)]
    #[case("pqr3stu8vwx", 38)]
    #[case("a1b2c3d4e5f", 15)]
    #[case("treb7uchet", 77)]
    #[case("two1nine", 29)]
    #[case("eightwothree", 83)]
    #[case("abcone2threexyz", 13)]
    #[case("xtwone3four", 24)]
    #[case("4nineeightseven2", 42)]
    #[case("zoneight234", 14)]
    #[case("7pqrstsixteen", 76)]
    fn should_calculate_number_for_line(#[case] sample: &str, #[case] expected: i32) {
        let result = handle_line(sample);
        assert_eq!(result, expected);
    }

    fn handle_line(sample: &str) -> i32 {
        let first_digit = get_first_digit(sample);
        let last_digit = get_last_digit(sample);

        first_digit * 10 + last_digit
    }

    #[test]
    fn day_1_example() {
        let input = read_file("day_1/sample.txt");
        let output = input.lines().into_iter().map(handle_line).sum();
        assert_eq!(142, output);
    }

    #[test]
    #[ignore] // No longer possible to verify part 1 after I changed impl
    fn day_1_part_1() {
        let input = read_file("day_1/part_1.txt");
        let output = input.lines().into_iter().map(handle_line).sum();
        assert_eq!(54630, output);
    }

    #[test]
    fn day_1_part_2() {
        let input = read_file("day_1/part_1.txt");
        let output = input.lines().into_iter().map(handle_line).sum();
        assert_eq!(54770, output);
    }
}

