use std::collections::HashMap;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{i32, space0, space1};
use nom::IResult;
use nom::multi::separated_list1;
use crate::day_2::Colour::{Blue, Green, Red};

#[derive(PartialEq, Eq, Hash, Debug)]
enum Colour {
    Blue,
    Red,
    Green,
}

#[derive(Debug, PartialEq)]
struct Hand {
    kubes: HashMap<Colour, i32>,
}

impl Hand {
    fn is_possible(&self) -> bool {
        self.kubes.get(&Red).map(|count| { *count <= 12 }).unwrap_or(true) &&
            self.kubes.get(&Green).map(|count| { *count <= 13 }).unwrap_or(true) &&
            self.kubes.get(&Blue).map(|count| { *count <= 14 }).unwrap_or(true)
    }

    fn count_number_of_colour(&self, colour: Colour) -> i32 {
        *self.kubes.get(&colour).unwrap_or(&0)
    }
}

#[derive(Debug, PartialEq)]
struct Game {
    number: i32,
    hands: Vec<Hand>,
}

impl Game {
    fn is_possible(&self) -> bool {
        self.hands.iter().all(Hand::is_possible)
    }


    fn find_minimal_cubes(&self) -> HashMap<Colour, i32> {
        HashMap::from([
            (Red, self.hands.iter().map(|hand| { hand.count_number_of_colour(Red) }).max().unwrap()),
            (Green, self.hands.iter().map(|hand| { hand.count_number_of_colour(Green) }).max().unwrap()),
            (Blue, self.hands.iter().map(|hand| { hand.count_number_of_colour(Blue) }).max().unwrap()),
        ])
    }

    fn calculate_minimal_power(&self) -> i32 {
        self.find_minimal_cubes().values().product()
    }
}

fn blue(input: &str) -> IResult<&str, Colour> {
    let (input, _) = tag("blue")(input)?;
    Ok((input, Blue))
}

fn red(input: &str) -> IResult<&str, Colour> {
    let (input, _) = tag("red")(input)?;
    Ok((input, Red))
}

fn green(input: &str) -> IResult<&str, Colour> {
    let (input, _) = tag("green")(input)?;
    Ok((input, Green))
}

fn kube_group(input: &str) -> IResult<&str, (Colour, i32)> {
    let (input, _) = space0(input)?;
    let (input, kube_count) = i32(input)?;
    let (input, _) = space1(input)?;
    let (input, colour) = alt((blue, red, green))(input)?;
    Ok((input, (colour, kube_count)))
}

fn hand(input: &str) -> IResult<&str, Hand> {
    let (input, list_of_groups) = separated_list1(tag(","), kube_group)(input)?;
    Ok((input, Hand {
        kubes: HashMap::from_iter(list_of_groups)
    }))
}

fn game(input: &str) -> IResult<&str, Game> {
    let (input, _) = tag("Game ")(input)?;
    let (input, game_number) = i32(input)?;
    let (input, _) = tag(":")(input)?;
    let (input, hands) = separated_list1(tag(";"), hand)(input)?;
    Ok((input, Game { number: game_number, hands }))
}

fn parse_game(input: &str) -> Game {
    let (_, game) = game(input).unwrap();
    game
}

fn solve_part_1(input: &str) -> i32 {
    input.lines().into_iter()
        .map(parse_game)
        .filter(Game::is_possible)
        .map(|game| { game.number })
        .sum()
}

fn solve_part_2(input: &str) -> i32 {
    input.lines().into_iter()
        .map(parse_game)
        .map(|game| {
            game.calculate_minimal_power()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use rstest::rstest;
    use crate::day_2::Colour::{Blue, Green, Red};
    use crate::day_2::{hand, kube_group, game, red, Game, Hand, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_red_stuff() {
        assert_eq!(red("red"), Ok(("", Red)))
    }

    #[test]
    fn should_parse_kube_group() {
        assert_eq!(kube_group("3 blue"), Ok(("", (Blue, 3))))
    }

    #[test]
    fn should_parse_hand() {
        assert_eq!(hand("3 blue, 4 red, 2 green"), Ok(("", Hand { kubes: HashMap::from([(Blue, 3), (Red, 4), (Green, 2)]) })));
    }

    #[rstest]
    #[case(
    "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
    Game{
    number: 1,
    hands: vec ! [
    Hand{ kubes: HashMap::from([(Blue, 3), (Red, 4)])},
    Hand{ kubes: HashMap::from([(Red, 1), (Green, 2), (Blue, 6)])},
    Hand{ kubes: HashMap::from([(Green, 2)])},
    ]}
    )]
    fn should_parse_game(#[case] sample: &str, #[case] expected: Game) {
        assert_eq!(game(sample), Ok(("", expected)));
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_2/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 8);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_2/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 2505);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_2/sample_part_1.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 2286);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_2/input.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 70265);
    }
}