use std::fmt::{Display, Formatter};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha0, newline, u64};
use nom::combinator::{all_consuming, map};
use nom::IResult;
use nom::multi::{many0, separated_list1};
use crate::day_15::Operation::{Add, Remove};

enum Operation {
    Remove,
    Add,
}

impl Display for Operation {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Remove => "-",
            Add => "=",
        })
    }
}

struct Step {
    label: String,
    operation: Operation,
    focal_length: Option<u64>,
}

impl Display for Step {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}{}{}",
            self.label,
            self.operation,
            self.focal_length
                .map(|val| { val.to_string() })
                .unwrap_or("".to_string())
        )
    }
}

fn operation(input: &str) -> IResult<&str, Operation> {
    let (input, operation) = alt((
        map(tag("="), |_| { Add }),
        map(tag("-"), |_| { Remove }),
    ))(input)?;
    Ok((input, operation))
}


fn step(input: &str) -> IResult<&str, Step> {
    let (input, label) = alpha0(input)?;
    let (input, operation) = operation(input)?;
    let (input, focal_lengths) = many0(u64)(input)?;

    let focal_length = if focal_lengths.is_empty() {
        None
    } else {
        Some(focal_lengths[0])
    };

    Ok((input, Step {
        label: label.to_string(),// don't need to mess with lifetimes...
        operation,
        focal_length,
    }))
}


fn hash_da_string(input: &str) -> u64 {
    input.chars().fold(0, |acc, next| {
        let ascii_val = next as u32;
        let current_value = acc + ascii_val as u64;
        let current_value = current_value * 17;
        let current_value = current_value % 256;
        current_value
    })
}

fn steps(input: &str) -> IResult<&str, Vec<Step>> {
    let (input, steps) = separated_list1(tag(","), step)(input)?;
    let (input, _) = newline(input)?;
    Ok((input, steps))
}

fn parse_steps(input: &str) -> Vec<Step> {
    let (_, steps) = all_consuming(steps)(input).unwrap();
    steps
}

fn solve_part_1(input: &str) -> u64 {
    let steps = parse_steps(input);
    let all_scores: Vec<u64> = steps.iter().map(|step| { format!("{step}") }).map(|step_string| hash_da_string(&step_string)).collect();
    //println!("{all_scores:?}");
    all_scores.iter().sum()
}

#[derive(Debug)]
struct Slot {
    label: String,
    focal_length: u64,
}

fn solve_part_2(input: &str) -> u64 {
    let steps = parse_steps(input);

    let mut boxes: Vec<Vec<Slot>> = (0..256).map(|_| Vec::new()).collect();


    for step in steps {
        let label_hash = hash_da_string(&step.label) as usize;

        let current_box = boxes.get_mut(label_hash).unwrap();
        let label_index_in_box = current_box.iter().position(|slot| step.label == slot.label);

        match label_index_in_box {
            None => {
                match step.operation {
                    Remove => {
                        //println!("NOOP for {step}");
                    },
                    Add => {
                        //println!("Inserting {step}");
                        current_box
                            .push(Slot { label: step.label.clone(), focal_length: step.focal_length.unwrap() })
                    },
                }
            }
            Some(index) => {
                match step.operation {
                    Remove => {
                        //println!("Removing for {step}");
                        current_box.remove(index);
                    },
                    Add => {
                        //println!("Replacing for {step}");
                        current_box.remove(index);
                        current_box.insert(index, Slot { label: step.label.clone(), focal_length: step.focal_length.unwrap()
                        });},
                }
            }
        }
        //println!("{boxes:?}");
    }

    //println!("{boxes:?}");

    boxes.iter().enumerate().map(|(box_number,current_box)| {
        current_box.iter().enumerate().map(move |(slot_index, current_slot)| {
            let box_score = 1 + (box_number as u64);
            let slot_score = (slot_index as u64) + 1;
            let result = box_score * slot_score * current_slot.focal_length;
            //println!("{} * {} * {} * {} = {}", current_slot.label, box_score, slot_score, current_slot.focal_length, result );
            result
        })
    }).flatten().sum()
}

#[cfg(test)]
mod tests {
    use crate::day_15::{hash_da_string, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_roll_all_rounded_rocks_north() {
        let input = "HASH";
        assert_eq!(hash_da_string(input), 52);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_15/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 1320);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_15/input.txt");
        assert_eq!(solve_part_1(&input), 509784);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_15/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 145);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_15/input.txt");
        assert_eq!(solve_part_2(&input), 230197);
    }
}