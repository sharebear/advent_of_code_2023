use std::fmt::{Display, Formatter};
use itertools::Itertools;
use pathfinding::prelude::astar;
use Direction::{Left, Up};
use crate::day_17::Direction::{Down, Right};

struct Map {
    city_blocks: Vec<Vec<u32>>,
}

#[derive(Eq, PartialEq, Hash, Clone, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Eq, PartialEq, Hash, Clone, Debug)] // Using derive for now, but a little unsure if we're allowed to progress through the same place with a different direction or not...
struct PosWithDirection {
    position: (usize, usize),
    direction: Option<Direction>, // None == Start position
}

impl Map {
    fn calculate_total_heat_loss(&self, target_position: &PosWithDirection, increment: usize) -> u32 {
        ////println!("{target_position:?} {increment}");
        let (target_x, target_y) = target_position.position;
        // Assume bounds checks have already been done....
        match &target_position.direction {
            None => panic!("Should always have a direction for a target (this is a sign my model could be improved)"),
            Some(direction) => match direction {
                Up => (0..increment).map(|current_increment| {
                    self.city_blocks[target_y + current_increment][target_x]
                }).sum(),
                Down => (0..increment).map(|current_increment| {
                    self.city_blocks[target_y - current_increment][target_x]
                }).sum(),
                Left => (0..increment).map(|current_increment| {
                    self.city_blocks[target_y][target_x + current_increment]
                }).sum(),
                Right => (0..increment).map(|current_increment| {
                    self.city_blocks[target_y][target_x - current_increment]
                }).sum(),
            }
        }
    }

    fn find_successors(&self, current_position: &PosWithDirection, minimum_moves: usize, maximum_moves: usize) -> Vec<(PosWithDirection, u32)> {
        let (current_x, current_y) = current_position.position;
        let max_x = self.city_blocks[0].len();
        let max_y = self.city_blocks.len();
        match &current_position.direction {
            None => (minimum_moves..=maximum_moves).flat_map(|increment| {
                let mut possible_successors = Vec::new();
                if current_x + increment < max_x {
                    let next_position = PosWithDirection { position: (current_x + increment, current_y), direction: Some(Right) };
                    //println!("{current_position:?}, {next_position:?}, {increment}");
                    possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                }

                if current_y + increment < max_y {
                    let next_position = PosWithDirection { position: (current_x, current_y + increment), direction: Some(Down) };
                    //println!("{current_position:?}, {next_position:?}, {increment}");
                    possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                }

                possible_successors
            }).collect(),
            Some(direction) => match direction {
                // If we were travelling up, we now need to go left or right 1-3 times if we don't hit the border
                Up => (minimum_moves..=maximum_moves).flat_map(|increment| {
                    let mut possible_successors = Vec::new();
                    if current_x as i32 - increment as i32 >= 0 {
                        let next_position = PosWithDirection { position: (current_x - increment, current_y), direction: Some(Left) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }
                    if current_x + increment < max_x {
                        let next_position = PosWithDirection { position: (current_x + increment, current_y), direction: Some(Right) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }

                    possible_successors
                }).collect(),
                Down => (minimum_moves..=maximum_moves).flat_map(|increment| {
                    let mut possible_successors = Vec::new();
                    if current_x as i32 - increment as i32 >= 0 {
                        let next_position = PosWithDirection { position: (current_x - increment, current_y), direction: Some(Left) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }
                    if current_x + increment < max_x {
                        let next_position = PosWithDirection { position: (current_x + increment, current_y), direction: Some(Right) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }

                    possible_successors
                }).collect(),
                Left => (minimum_moves..=maximum_moves).flat_map(|increment| {
                    let mut possible_successors = Vec::new();
                    if current_y as i32 - increment as i32 >= 0 {
                        let next_position = PosWithDirection { position: (current_x, current_y - increment), direction: Some(Up) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }
                    if current_y + increment < max_y {
                        let next_position = PosWithDirection { position: (current_x, current_y + increment), direction: Some(Down) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }

                    possible_successors
                }).collect(),
                Right => (minimum_moves..=maximum_moves).flat_map(|increment| {
                    let mut possible_successors = Vec::new();
                    if (current_y as i32 - increment as i32) >= 0 {
                        let next_position = PosWithDirection { position: (current_x, current_y - increment), direction: Some(Up) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }
                    if current_y + increment < max_y {
                        let next_position = PosWithDirection { position: (current_x, current_y + increment), direction: Some(Down) };
                        //println!("{current_position:?}, {next_position:?}, {increment}");
                        possible_successors.push((next_position.clone(), self.calculate_total_heat_loss(&next_position, increment)));
                    }

                    possible_successors
                }).collect(),
            }
        }
    }

    fn calculate_heuristic(&self, current_position: &(usize, usize), end_position: &(usize, usize)) -> u32 {
        (current_position.0.abs_diff(end_position.0) + current_position.1.abs_diff(end_position.1)) as u32
    }

    fn find_minimal_route_cost(&self, minimum_moves: usize, maximum_moves: usize) -> u32 {
        let initial_position = PosWithDirection { position: (0, 0), direction: None };
        let end_position = (self.city_blocks[0].len() - 1, self.city_blocks.len() - 1);
        astar(
            &initial_position,
            |current_position| {
                self.find_successors(current_position, minimum_moves, maximum_moves)
            },
            |current_position| {
                self.calculate_heuristic(&current_position.position, &end_position)
            },
            |current_position| {
                current_position.position.eq(&end_position)
            })
            .expect("Found no route!")
            .1
    }
}

impl Display for Map {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}\n",
            self.city_blocks
                .iter()
                .map(|line| {
                    line
                        .iter()
                        .map(|block| block.to_string())
                        .join("")
                })
                .join("\n")
        )
    }
}

fn parse_map(input: &str) -> Map {
    let city_blocks: Vec<Vec<u32>> = input
        .lines()
        .filter(|line| !line.is_empty())
        .map(|line| line
            .chars()
            .map(|char| char.to_digit(10).unwrap())
            .collect()
        ).collect();
    Map { city_blocks }
}

fn solve_part_1(input: &str) -> u32 {
    let map = parse_map(input);
    map.find_minimal_route_cost(1, 3)
}

fn solve_part_2(input: &str) -> u32 {
    let map = parse_map(input);
    map.find_minimal_route_cost(4, 10)
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use crate::day_17::{parse_map, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_and_render_map() {
        let input = indoc! {r"
            2413432311323
            3215453535623
            3255245654254
            3446585845452
            4546657867536
            1438598798454
            4457876987766
            3637877979653
            4654967986887
            4564679986453
            1224686865563
            2546548887735
            4322674655533
        "};

        assert_eq!(parse_map(input).to_string(), input);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_17/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 102);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_17/input.txt");
        assert_eq!(solve_part_1(&input), 942);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_17/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 94);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_17/input.txt");
        assert_eq!(solve_part_2(&input), 1082);
    }
}