use std::fmt::Formatter;
use core::fmt::Display;
use std::cell::Cell;
use itertools::{Itertools};
use nom::branch::alt;
use nom::bytes::complete::{is_not, tag};
use nom::character::complete::{newline, space1, u32};
use nom::combinator::{map};
use nom::IResult;
use nom::multi::{many0, separated_list1};
use nom::sequence::delimited;
use crate::day_18::Direction::{Down, Left, Right, Up};
use crate::day_18::Plot::{Terrain, Trench};

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Display for Direction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Up => "U",
            Down => "D",
            Left => "L",
            Right => "R",
        })
    }
}

struct PlanStep {
    direction: Direction,
    distance: u32,
    colour: String, // Temp, lets see how part 2 looks
}

impl Display for PlanStep {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} (#{})", self.direction, self.distance, self.colour)
    }
}

#[derive(Debug)]
struct DigPlanMetadata {
    width: u32,
    height: u32,
    start_point: (u32, u32),
}

struct DigPlan {
    steps: Vec<PlanStep>,
}

impl Display for DigPlan {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}\n", self.steps.iter().map(|step|step.to_string()).join("\n"))
    }
}

impl DigPlan {
    fn calculate_vertices(&self) -> Vec<(i32, i32)> {
        let mut vertices = Vec::new();
        let mut current_vertex = (0i32,0i32);
        vertices.push(current_vertex);

        for step in &self.steps {
            current_vertex = match step.direction {
                Up => (current_vertex.0, current_vertex.1 - step.distance as i32),
                Down => (current_vertex.0, current_vertex.1 + step.distance as i32),
                Left => (current_vertex.0 - step.distance as i32, current_vertex.1),
                Right => (current_vertex.0 + step.distance as i32, current_vertex.1),
            };
            vertices.push(current_vertex);
        }

        vertices
    }

    fn _calculate_area(&self) -> i32 {
        // REF: https://www.wikihow.com/Calculate-the-Area-of-a-Polygon
        // This didn't work, after some hours sleep I think I know why, but I don't have time to
        // try and fix right now.
        let vertices = self.calculate_vertices();

        let first_products:i32 = (0..vertices.len()-1).map(|index| {
            vertices[index].0 * vertices[index + 1].1
        }).sum();

        let second_products:i32 = (0..vertices.len()-1).map(|index| {
            vertices[index].1 * vertices[index + 1].0
        }).sum();

        let result = (first_products - second_products) / 2;

        //println!("{first_products}, {second_products}, {result}");

        result
    }

    fn calculate_bounds(&self) -> DigPlanMetadata {
        let vertices = self.calculate_vertices();

        println!("{:?}", vertices);


        let max_y = vertices.iter().map(|vertex| vertex.1).max().unwrap();
        let max_x = vertices.iter().map(|vertex| vertex.0).max().unwrap();
        let min_y = vertices.iter().map(|vertex| vertex.1).min().unwrap();
        let min_x = vertices.iter().map(|vertex| vertex.0).min().unwrap();

        println!("{} {}", min_y.abs(), max_y.abs());

        DigPlanMetadata{
            width: (min_x.abs() + max_x.abs() + 1) as u32,
            height: (min_y.abs() + max_y.abs() + 2) as u32,
            start_point: (min_x.abs() as u32, min_y.abs() as u32),
        }
    }
}

#[derive(Copy, Clone, PartialEq)]
enum Plot {
    Terrain,
    Trench,
}

impl Display for Plot {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Terrain => ".",
            Trench => "#",
        })
    }
}

struct DigSite {
    plots: Vec<Vec<Cell<Plot>>>,
}

impl Display for DigSite {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}\n", self.plots.iter().map(|row| row.iter().map(|cell|cell.get().to_string()).join("")).join("\n"))
    }
}

impl DigSite {
    fn with_dimensions(width: u32, height:u32) -> Self {
        let plots: Vec<Vec<Cell<Plot>>> = (0..height).map(|_| {
            (0..width).map(|_|{
                Cell::new(Terrain)
            }).collect()
        }).collect();

        DigSite{ plots }
    }

    fn dig(&self, x:u32, y:u32) {
        self.plots[y as usize][x as usize].replace(Trench);
    }

    fn dig_perimiter(&self, start_point: (u32, u32), dig_plan: DigPlan) {
        let mut current_x = start_point.0;
        let mut current_y = start_point.1;
        self.dig(current_x, current_y);
        for step in dig_plan.steps {
            match step.direction {
                Up => {
                    let target_y = current_y - step.distance;
                    (target_y..current_y).for_each(|y|self.dig(current_x, y));
                    current_y= target_y;
                }
                Down => {
                    let target_y = current_y + step.distance;
                    (current_y+1..=target_y).for_each(|y|self.dig(current_x, y));
                    current_y = target_y;
                }
                Left => {
                    let target_x = current_x - step.distance;
                    (target_x..current_x).for_each(|x|self.dig(x, current_y));
                    current_x = target_x;
                }
                Right => {
                    println!("{step}");
                    //println!("{self}");
                    let target_x = current_x + step.distance;
                    (current_x+1..=target_x).for_each(|x| self.dig(x, current_y));
                    current_x = target_x;
                }
            }
            //println!("{self}");
        }
    }

    fn calculate_capacity(&self) -> u32 {
        self.plots.iter().map(|row|{
            row.iter().map(|cell| {
                match cell.get() {
                    Terrain => 0,
                    Trench => 1,
                }
            }).sum::<u32>()
        }).sum()
    }

    fn dig_interior(&self, start_x:u32, start_y:u32) {
        //println!("{self}");
        self.dig(start_x, start_y);
        let surrounding_diff:Vec<(i32, i32)> = vec![
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ];
        let mut plots_to_iterate: Vec<(u32, u32)> = surrounding_diff.iter().filter_map(|(diff_x, diff_y)| {
            let target_x = (start_x as i32 + diff_x) as u32;
            let target_y = (start_y as i32 + diff_y) as u32;
            let target = self.plots[target_y as usize][target_x as usize].get();
            //println!("Checking target ({target_x}, {target_y}) [{target}] for start {start_x}, {start_y}");
            if target == Terrain {
                Some((target_x, target_y))
            } else { None }
        }).collect();

        while !plots_to_iterate.is_empty() {
            let (x, y) = plots_to_iterate.pop().unwrap();
            self.dig(x, y);
            let mut next_things: Vec<(u32,u32)> = surrounding_diff.iter().filter_map(|(diff_x, diff_y)| {
                let target_x = (x as i32 + diff_x) as u32;
                let target_y = (y as i32 + diff_y) as u32;
                let target = self.plots[target_y as usize][target_x as usize].get();
                //println!("Checking target ({target_x}, {target_y}) [{target}] for start {start_x}, {start_y}");
                if target == Terrain {
                    Some((target_x, target_y))
                } else { None }
            }).collect();
            //println!("{next_things:?}");
            plots_to_iterate.append(&mut next_things);
        }


    }
}

fn direction(input: &str) -> IResult<&str, Direction> {
    let (input, direction) = alt((
        map(tag("U"), |_| { Up }),
        map(tag("D"), |_| { Down }),
        map(tag("L"), |_| { Left }),
        map(tag("R"), |_| { Right }),
    ))(input)?;
    Ok((input, direction))
}

fn plan_step(input: &str) -> IResult<&str, PlanStep> {
    let (input, direction) = direction(input)?;
    let (input, _) = space1(input)?;
    let (input, distance) = u32(input)?;
    let (input, _) = space1(input)?;
    let (input, colour) = delimited(tag("(#"), is_not(")"), tag(")"))(input)?;

    Ok((input, PlanStep {
        direction,
        distance,
        colour: colour.to_string(),
    }))
}

fn dig_plan(input: &str) -> IResult<&str, DigPlan> {
    let (input, steps) = separated_list1(newline, plan_step)(input)?;
    let (input, _) = many0(newline)(input)?;
    Ok((input, DigPlan { steps }))
}

fn parse_digplan(input: &str) -> DigPlan {
    let (_, dig_plan) = dig_plan(input).unwrap();
    dig_plan
}

fn solve_part_1(input: &str) -> u32 {
    let dig_plan = parse_digplan(input);
    let plan_metadata = dig_plan.calculate_bounds();
    println!("{plan_metadata:?}");
    let dig_site = DigSite::with_dimensions(plan_metadata.width, plan_metadata.height);
    dig_site.dig_perimiter(plan_metadata.start_point, dig_plan);
    println!("{dig_site}");
    dig_site.dig_interior(31, 7);
    println!("{dig_site}");
    dig_site.calculate_capacity()
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use crate::day_18::{DigSite, parse_digplan, solve_part_1};
    use crate::read_file;

    #[test]
    fn should_parse_and_render_example() {
        let input = indoc! {r"
            R 6 (#70c710)
            D 5 (#0dc571)
            L 2 (#5713f0)
            D 2 (#d2c081)
            R 2 (#59c680)
            D 2 (#411b91)
            L 5 (#8ceee2)
            U 2 (#caa173)
            L 1 (#1b58a2)
            U 2 (#caa171)
            R 2 (#7807d2)
            U 3 (#a77fa3)
            L 2 (#015232)
            U 2 (#7a21e3)
        "};
        assert_eq!(parse_digplan(input).to_string(), input);
    }

    #[test]
    fn should_calculate_dig_vertices() {
        let input = indoc! {r"
            R 6 (#70c710)
            D 5 (#0dc571)
            L 2 (#5713f0)
            D 2 (#d2c081)
            R 2 (#59c680)
            D 2 (#411b91)
            L 5 (#8ceee2)
            U 2 (#caa173)
            L 1 (#1b58a2)
            U 2 (#caa171)
            R 2 (#7807d2)
            U 3 (#a77fa3)
            L 2 (#015232)
            U 2 (#7a21e3)
        "};
        // Assuming for now that if we use 0,0 as top left corner we don't need to do negative stuff
        let expected = vec![
            (0,0),
            (6,0), // R 6 (#70c710)
            (6,5), // D 5 (#0dc571)
            (4,5), // L 2 (#5713f0)
            (4,7), // D 2 (#d2c081)
            (6,7), // R 2 (#59c680)
            (6,9), // D 2 (#411b91)
            (1,9), // L 5 (#8ceee2)
            (1,7), // U 2 (#caa173)
            (0,7), // L 1 (#1b58a2)
            (0,5), // U 2 (#caa171)
            (2,5), // R 2 (#7807d2)
            (2,2), // U 3 (#a77fa3)
            (0,2), // L 2 (#015232)
            (0,0), // U 2 (#7a21e3)
        ];

        let dig_plan = parse_digplan(input);

        assert_eq!(dig_plan.calculate_vertices(), expected);
    }

    #[test]
    fn should_calculate_dig_perimeter_capacity() {
        let input = indoc! {r"
            R 6 (#70c710)
            D 5 (#0dc571)
            L 2 (#5713f0)
            D 2 (#d2c081)
            R 2 (#59c680)
            D 2 (#411b91)
            L 5 (#8ceee2)
            U 2 (#caa173)
            L 1 (#1b58a2)
            U 2 (#caa171)
            R 2 (#7807d2)
            U 3 (#a77fa3)
            L 2 (#015232)
            U 2 (#7a21e3)
        "};

        let dig_plan = parse_digplan(input);
        let dig_plan_metadata = dig_plan.calculate_bounds();

        let dig_site = DigSite::with_dimensions(dig_plan_metadata.width, dig_plan_metadata.height);
        dig_site.dig_perimiter(dig_plan_metadata.start_point, dig_plan);

        assert_eq!(dig_site.calculate_capacity(), 38);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_18/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 62);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_18/input.txt");
        assert_eq!(solve_part_1(&input), 53844);
    }
}
