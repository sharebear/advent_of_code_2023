use std::cell::Cell;
use itertools::Itertools;
use num::Integer;
use crate::day_10::Direction::{East, North, South, West};
use crate::day_10::TileType::{Ground, HorizontalPipe, NorthWestBend, NorthEastBend, SouthEastBend, SouthWestBend, Start, VerticalPipe};

#[derive(PartialEq, Debug)]
enum TileType {
    VerticalPipe,
    HorizontalPipe,
    NorthEastBend,
    NorthWestBend,
    SouthWestBend,
    SouthEastBend,
    Ground,
    Start,
}

#[derive(PartialEq, Debug)]
struct Tile {
    tile_type: TileType,
    is_visited: Cell<bool>,
    is_inside_tile: Cell<bool>,
}

impl Tile {
    fn next_direction(&self, current_direction: &Direction) -> Direction {
        //println!("self: {:?}, currend_direction: {:?}", self, current_direction);
        match self.tile_type {
            VerticalPipe => match current_direction {
                North => North,
                South => South,
                _ => panic!("Impossible move"),
            },
            HorizontalPipe => match current_direction {
                East => East,
                West => West,
                _ => panic!("Impossible move"),
            }
            NorthEastBend => match current_direction {
                South => East,
                West => North,
                _ => panic!("Impossible move"),
            }
            NorthWestBend => match current_direction {
                South => West,
                East => North,
                _ => panic!("Impossible move"),
            }
            SouthWestBend => match current_direction {
                North => West,
                East => South,
                _ => panic!("Impossible move"),
            }
            SouthEastBend => match current_direction {
                North => East,
                West => South,
                _ => panic!("Impossible move"),
            }
            Ground => panic!("Impossible start point for moving"),
            Start => panic!("Impossible start point for moving"),
        }
    }

    fn mark_visited(&self) -> () {
        self.is_visited.replace(true);
    }

    fn mark_inside_tile(&self) {
        if self.is_inside_tile.get() != true {
            self.is_inside_tile.replace(true);
        }
    }

    fn passed_string(&self) -> &str {
        if self.is_visited.get() {
            match self.tile_type {
                VerticalPipe => "║",
                HorizontalPipe => "═",
                NorthEastBend => "╚",
                NorthWestBend => "╝",
                SouthWestBend => "╗",
                SouthEastBend => "╔",
                Ground => panic!("Can't visit ground"),
                Start => "S"
            }
        } else if self.is_inside_tile.get() {
            "I"
        } else {
            "."
        }
    }
}

#[derive(PartialEq, Debug)]
struct TileGrid {
    tiles: Vec<Vec<Tile>>,
}

impl TileGrid {
    fn find_start_position(&self) -> (usize, usize) {
        for y in 0..self.tiles.len() {
            let row = self.tiles.iter().nth(y).unwrap();
            for x in 0..row.len() {
                if self.tiles[y][x].tile_type == Start {
                    return (x, y);
                }
            }
        }
        panic!("Couldn't find start");
    }

    fn find_start_directions(&self) -> Vec<Direction> {
        let start_position = self.find_start_position();

        let mut start_directions = Vec::new();

        if start_position.1 > 0 {
            let north_tile = &self.tiles[start_position.1 - 1][start_position.0];
            start_directions.push(match north_tile.tile_type {
                VerticalPipe => Some(North),
                SouthWestBend => Some(North),
                SouthEastBend => Some(North),
                _ => None,
            });
        }

        if start_position.1 < self.tiles.len() {
            let south_tile = &self.tiles[start_position.1 + 1][start_position.0];
            start_directions.push(match &south_tile.tile_type {
                VerticalPipe => Some(South),
                NorthWestBend => Some(South),
                NorthEastBend => Some(South),
                _ => None,
            });
        }

        if start_position.0 > 0 {
            let west_tile = &self.tiles[start_position.1][start_position.0 - 1];
            start_directions.push(match &west_tile.tile_type {
                HorizontalPipe => Some(West),
                SouthEastBend => Some(West),
                NorthEastBend => Some(West),
                _ => None,
            });
        }

        if start_position.0 < self.tiles[0].len() {
            let east_tile = &self.tiles[start_position.1][start_position.0 + 1];
            start_directions.push(match &east_tile.tile_type {
                HorizontalPipe => Some(East),
                NorthWestBend => Some(East),
                SouthWestBend => Some(East),
                _ => None,
            });
        }

        start_directions.into_iter().flatten().collect()
    }

    fn find_loop_length(&self) -> u64 {
        let mut current_position = self.find_start_position();
        let first_direction = self.find_start_directions()[0].clone();
        println!("First direction: {:?}", first_direction);
        let mut move_count = 0u64;

        let mut current_direction = first_direction;

        loop {
            move_count += 1;
            current_position = match current_direction {
                North => (current_position.0, current_position.1 - 1),
                South => (current_position.0, current_position.1 + 1),
                East => (current_position.0 + 1, current_position.1),
                West => (current_position.0 - 1, current_position.1),
            };
            let current_tile = &self.tiles[current_position.1][current_position.0];
            current_tile.mark_visited();
            if current_tile.tile_type == Start {
                break;
            }
            current_direction = current_tile.next_direction(&current_direction);
        }

        move_count
    }

    fn passed_string(&self) -> String {
        self.tiles.iter().map(|row| {
            row.iter().map(|tile| {
                tile.passed_string()
            }).join("")
        }).join("\n")
    }

    fn is_point_in_loop(&self, x: usize, y: usize) -> bool {
        let result = if self.tiles[y][x].is_visited.get() {
            //println!("Evaluating point ({:?}, {:?}), has been visited", x, y);
            false
        } else {
            let count = self.tiles[y][x + 1..].iter().map(|tile| {
                if !tile.is_visited.get() {
                    return false;
                }
                let evaluation =

                    match tile.tile_type {
                        HorizontalPipe => false,
                        NorthWestBend => false,
                        NorthEastBend => false,
                        Ground => false,
                        _ => true,
                    };
                //println!("Evaluating point ({:?}, {:?}), type {:?}, result {:?}", x, y, tile.tile_type, evaluation);
                evaluation
            }).filter(|is_border| { *is_border == true })
                .count();
            //println!("count: {:?}", count);
            count.is_odd()
        };
        if result {
            self.tiles[y][x].mark_inside_tile();
        }
        result
    }

    fn count_tiles_in_loop(&self) -> u64 {
        let mut tile_counter = 0u64;
        for y in 0..self.tiles.len() {
            for x in 0..self.tiles[0].len() {
                let is_point_in_loop = self.is_point_in_loop(x, y);
                if is_point_in_loop {
                    tile_counter += 1;
                }
            }
        }
        tile_counter
    }
}

#[derive(PartialEq, Debug, Clone)]
enum Direction {
    North,
    South,
    East,
    West,
}

fn parse_row(input: &str) -> Vec<Tile> {
    input.trim().chars().map(|char| {
        let tile_type = match char {
            '|' => VerticalPipe,
            '-' => HorizontalPipe,
            'L' => TileType::NorthEastBend,
            'J' => NorthWestBend,
            '7' => SouthWestBend,
            'F' => SouthEastBend,
            '.' => Ground,
            'S' => Start,
            _ => { panic!("Should not happen") }
        };
        Tile {
            tile_type,
            is_visited: Cell::new(false),
            is_inside_tile: Cell::new(false),
        }
    }).collect()
}

fn parse_tile_grid(input: &str) -> TileGrid {
    let tiles = input.lines()
        .filter(|line| { !line.trim().is_empty() })
        .map(parse_row).collect();
    TileGrid { tiles }
}

fn solve_part_1(input: &str) -> u64 {
    let tile_grid = parse_tile_grid(input);
    let loop_length = tile_grid.find_loop_length();
    loop_length / 2
}

fn solve_part_2(input: &str) -> u64 {
    let tile_grid = parse_tile_grid(input);
    let _ = tile_grid.find_loop_length();
    //tile_grid.visited_string().lines().for_each(|line| println!("{:?}", line));
    let tile_count = tile_grid.count_tiles_in_loop();
    tile_grid.passed_string().lines().for_each(|line| println!("{:?}", line));
    tile_count
}

#[cfg(test)]
mod tests {
    use std::cell::Cell;
    use crate::day_10::{parse_row, parse_tile_grid, solve_part_1, solve_part_2, Tile, TileGrid};
    use crate::day_10::Direction::{East, South};
    use crate::day_10::TileType::{Ground, HorizontalPipe, NorthEastBend, NorthWestBend, SouthEastBend, SouthWestBend, Start, VerticalPipe};
    use crate::read_file;

    #[test]
    fn should_parse_line() {
        let input = "-L|F7";
        assert_eq!(parse_row(input), vec![
            Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false), is_inside_tile: Cell::new(false) },
            Tile { tile_type: NorthEastBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) },
            Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) },
            Tile { tile_type: SouthEastBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) },
            Tile { tile_type: SouthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) },
        ]);
    }

    #[test]
    fn should_parse_grid() {
        let input = r#"
        .....
        .S-7.
        .|.|.
        .L-J.
        .....
        "#;
        assert_eq!(parse_tile_grid(input), TileGrid {
            tiles: vec![
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Start, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: SouthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthEastBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false), is_inside_tile: Cell::new(false) }],
            ]
        });
    }

    #[test]
    fn should_find_start_postition() {
        let input = TileGrid {
            tiles: vec![
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Start, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: SouthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthEastBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false), is_inside_tile: Cell::new(false) }],
            ]
        };
        assert_eq!(input.find_start_position(), (1, 1));
    }

    #[test]
    fn should_find_possible_start_directions() {
        let input = TileGrid {
            tiles: vec![
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Start, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: SouthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthEastBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false), is_inside_tile: Cell::new(false) }],
            ]
        };
        assert_eq!(input.find_start_directions(), vec![South, East]);
    }

    #[test]
    fn should_find_loop_length() {
        let input = TileGrid {
            tiles: vec![
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Start, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: SouthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: VerticalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthEastBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: HorizontalPipe, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: NorthWestBend, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }],
                vec![Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false),  is_inside_tile: Cell::new(false) }, Tile { tile_type: Ground, is_visited: Cell::new(false), is_inside_tile: Cell::new(false) }],
            ]
        };
        assert_eq!(input.find_loop_length(), 8);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_10/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 8);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_10/input.txt");
        assert_eq!(solve_part_1(&input), 6754);
    }


    #[test]
    fn should_solve_part_2_with_simplified() {
        let input = read_file("day_10/simplified.txt");
        assert_eq!(solve_part_2(&input), 1);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_10/sample_part_2.txt");
        assert_eq!(solve_part_2(&input), 10);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_10/input.txt");
        assert_eq!(solve_part_2(&input), 567);
    }
}