use std::cmp::Ordering;
use std::cmp::Ordering::{Equal, Greater, Less};
use itertools::{Diff, diff_with, Itertools};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{multispace1, u32};
use nom::combinator::map;
use nom::IResult;
use nom::multi::{many1};
use CardsStrength::{FiveOfAKind, FourOfAKind, HighCard};
use crate::day_7::CardsStrength::{FullHouse, OnePair, ThreeOfAKind, TwoPair};

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
enum Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    //Jack,
    Queen,
    King,
    Ace,
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Card {
    fn cmp(&self, other: &Self) -> Ordering {
        (*self as u8).cmp(&(*other as u8))
    }
}

#[derive(PartialEq, Eq, Debug)]
struct Hand {
    cards: Vec<Card>,
    bet: u32,
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        let self_strength = calculate_cards_strength(&self.cards);
        let other_strength = calculate_cards_strength(&other.cards);
        let first_order = self_strength.cmp(&other_strength);
        if first_order == Equal {
            let diff = diff_with(self.cards.clone(), other.cards.clone(), |i, j| {
                i == j
            }).unwrap();
            match diff {
                Diff::FirstMismatch(_, i, j) => {j.cmp(i)}
                Diff::Shorter(_, _) => { panic!("Shouldn't happen")}
                Diff::Longer(_, _) => {panic!("Shouldn't happen")}
            }
        } else {
            first_order
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(PartialEq, Eq, Debug)]
enum CardsStrength {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl Ord for CardsStrength {
    fn cmp(&self, other: &Self) -> Ordering {
        compare_cards_strength(self, other)
    }
}

impl PartialOrd for CardsStrength {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}


fn calculate_cards_strength(input: &Vec<Card>) -> CardsStrength {
    let mut initial_counts = input.into_iter().counts();
    let mut print_debug = false;
    if initial_counts.get(&Card::Joker).map(|val| {val == &5}).unwrap_or(false) {
        print_debug = true;
        println!("Before: {:?}", initial_counts);
    }

    let number_of_jokers = initial_counts.remove(&Card::Joker).unwrap_or(0usize);
    let mut cards_with_counts = vec![];
    for (k, v) in initial_counts.clone().into_iter() {
        cards_with_counts.push((k, v));
    }

    cards_with_counts.sort_by(|this, other| {
        let first = this.1.cmp(&other.1);
        if first == Equal {
            this.0.cmp(&other.0)
        } else {
            first
        }
    });

    let most_card = cards_with_counts.last().unwrap_or(&(&Card::Ace, 0usize)).0;

    if number_of_jokers == 5 {
        initial_counts.insert(&Card::Ace, 5);
    } else {
        *initial_counts.get_mut(most_card).unwrap() += number_of_jokers;
    }

    if print_debug {
        println!("After: {:?}", initial_counts);
    }
    let counts: Vec<usize> = initial_counts.into_values().collect();
    //println!("{:?}", counts);
    if counts.contains(&5usize) {
        FiveOfAKind
    } else if counts.contains(&4usize) {
        FourOfAKind
    } else if counts.contains(&3usize) && counts.contains(&2usize) {
        FullHouse
    } else if counts.contains(&3usize) {
        ThreeOfAKind
    } else if counts.iter().counts().get(&2usize).map( |pair_count| {pair_count == &2usize}).unwrap_or(false) {
        TwoPair
    }else if counts.contains(&2usize) {
        OnePair
    } else {
        HighCard
    }
}

fn compare_cards_strength(a: &CardsStrength, b: &CardsStrength) -> Ordering {
    match a {
        HighCard => {
            match b {
                HighCard => {
                    Equal
                }
                OnePair => {
                    Greater
                }
                TwoPair => {
                    Greater
                }
                ThreeOfAKind => { Greater}
                FullHouse => { Greater}
                FourOfAKind => { Greater}
                FiveOfAKind => {Greater}
            }
        }
        OnePair => {
            match b {
                HighCard => { Less}
                OnePair => { Equal}
                TwoPair => {Greater}
                ThreeOfAKind => {Greater}
                FullHouse => {Greater}
                FourOfAKind => {Greater}
                FiveOfAKind => {Greater}
            }
        }
        TwoPair => {
            match b {
                HighCard => {Less}
                OnePair => {Less}
                TwoPair => {Equal}
                ThreeOfAKind => {Greater}
                FullHouse => {Greater}
                FourOfAKind => {Greater}
                FiveOfAKind => {Greater}
            }
        }
        ThreeOfAKind => {
            match b {
                HighCard => {Less}
                OnePair => {Less}
                TwoPair => {Less}
                ThreeOfAKind => {Equal}
                FullHouse => {Greater}
                FourOfAKind => {Greater}
                FiveOfAKind => {Greater}
            }
        }
        FullHouse => {
            match b {
                HighCard => {Less}
                OnePair => {Less}
                TwoPair => {Less}
                ThreeOfAKind => {Less}
                FullHouse => {Equal}
                FourOfAKind => {Greater}
                FiveOfAKind => {Greater}
            }
        }
        FourOfAKind => {
            match b {
                HighCard => {Less}
                OnePair => {Less}
                TwoPair => {Less}
                ThreeOfAKind => {Less}
                FullHouse => {Less}
                FourOfAKind => {Equal}
                FiveOfAKind => {Greater}
            }
        }
        FiveOfAKind => {
            match b {
                HighCard => {Less}
                OnePair => {Less}
                TwoPair => {Less}
                ThreeOfAKind => {Less}
                FullHouse => {Less}
                FourOfAKind => {Less}
                FiveOfAKind => {Equal}
            }
        }
    }
}

fn card(input: &str) -> IResult<&str, Card> {
    let (input, card) = alt((
        map(tag("2"), |_| { Card::Two }),
        map(tag("3"), |_| { Card::Three }),
        map(tag("4"), |_| { Card::Four }),
        map(tag("5"), |_| { Card::Five }),
        map(tag("6"), |_| { Card::Six }),
        map(tag("7"), |_| { Card::Seven }),
        map(tag("8"), |_| { Card::Eight }),
        map(tag("9"), |_| { Card::Nine }),
        map(tag("T"), |_| { Card::Ten }),
        map(tag("J"), |_| { Card::Joker }),
        map(tag("Q"), |_| { Card::Queen }),
        map(tag("K"), |_| { Card::King }),
        map(tag("A"), |_| { Card::Ace }),
    ))(input)?;
    Ok((input, card))
}

fn hand(input: &str) -> IResult<&str, Hand> {
    let (input, cards) = many1(card)(input)?;
    let (input, _) = multispace1(input)?;
    let (input, bet) = u32(input)?;
    Ok((input, Hand { cards, bet }))
}

fn parse_hand(input: &str) -> Hand {
    let (_, hand) = hand(input).unwrap();
    hand
}

fn solve_part_1(input: &str) -> u32 {
    let mut sorted:Vec<Hand> = input.lines().into_iter().map(parse_hand).sorted().collect();
    sorted.reverse();

    sorted.iter().enumerate().map(|pair| {
        //println!("pos: {:?}, bet: {:?}, sdfjd {:?}", pair.0, pair.1.cards, calculate_cards_strength(&pair.1.cards));
        (pair.0 as u32 + 1) * pair.1.bet
    }).sum()
}

#[cfg(test)]
mod tests {
    use crate::day_7::{calculate_cards_strength, Hand, parse_hand, solve_part_1};
    use crate::day_7::Card::{Ace, Eight, Five, Four, King, Nine, Ten, Three, Two};
    use crate::day_7::CardsStrength::{FiveOfAKind, FourOfAKind, FullHouse, HighCard, OnePair, ThreeOfAKind, TwoPair};
    use crate::read_file;

    #[test]
    fn should_parse_hand() {
        let input = "32T3K 765";
        assert_eq!(
            parse_hand(input),
            Hand {
                cards: vec![
                    Three,
                    Two,
                    Ten,
                    Three,
                    King,
                ],
                bet: 765,
            }
        )
    }

    #[test]
    fn should_find_five_of_a_kind() {
        let input = vec![
            Ace,
            Ace,
            Ace,
            Ace,
            Ace,
        ];
        assert_eq!(calculate_cards_strength(&input), FiveOfAKind)
    }

    #[test]
    fn should_find_four_of_a_kind() {
        let input = vec![
            Ace,
            Ace,
            Eight,
            Ace,
            Ace,
        ];
        assert_eq!(calculate_cards_strength(&input), FourOfAKind)
    }

    #[test]
    fn should_find_full_house() {
        let input = vec![
            Two,
            Three,
            Three,
            Three,
            Two,
        ];
        assert_eq!(calculate_cards_strength(&input), FullHouse)
    }

    #[test]
    fn should_find_three_of_a_kind() {
        let input = vec![
            Ten,
            Ten,
            Ten,
            Nine,
            Eight,
        ];
        assert_eq!(calculate_cards_strength(&input), ThreeOfAKind)
    }
    #[test]
    fn should_find_two_pair() {
        let input = vec![
            Two,
            Three,
            Four,
            Three,
            Two,
        ];
        assert_eq!(calculate_cards_strength(&input), TwoPair)
    }

    #[test]
    fn should_find_one_pair() {
        let input = vec![
            Ace,
            Two,
            Three,
            Ace,
            Four,
        ];
        assert_eq!(calculate_cards_strength(&input), OnePair)
    }

    #[test]
    fn should_find_high_card() {
        let input = vec![
            Ace,
            Two,
            Three,
            Four,
            Five,
        ];
        assert_eq!(calculate_cards_strength(&input), HighCard)
    }

    #[test]
    #[ignore] // broken for solving part 2
    fn should_solve_part_1_example() {
        let input = read_file("day_7/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 6440);
    }

    #[test]
    #[ignore]// broken for solving part 2
    fn should_solve_part_1_real() {
        let input = read_file("day_7/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 251106089);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_7/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 5905);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_7/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 249620106);
    }
}