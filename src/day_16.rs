use std::cell::RefCell;
use std::fmt::{Display, Formatter};
use itertools::Itertools;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::newline;
use nom::combinator::map;
use nom::IResult;
use nom::multi::{many1, separated_list1};
use TileType::{EmptySpace, MirrorBackslash, MirrorSlash, SplitterHorizontal, SplitterVertical};
use crate::day_16::Direction::{Down, Left, Right, Up};

enum TileType {
    EmptySpace,
    MirrorSlash,
    MirrorBackslash,
    SplitterVertical,
    SplitterHorizontal,
}

impl Display for TileType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", format!("{}", match self {
            EmptySpace => ".",
            MirrorSlash => "/",
            MirrorBackslash => "\\",
            SplitterVertical => "|",
            SplitterHorizontal => "-",
        }))
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Display for Direction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Left => "<",
            Right => ">",
            Up => "^",
            Down => "v",
        })
    }
}

struct Tile {
    content: TileType,
    visits: RefCell<Vec<Direction>>,
}

impl Tile {
    fn to_bounces_string(&self) -> String {
        let visits = self.visits.borrow();
        let num_visits = visits.len();

        format!("{}", match self.content {
            EmptySpace => if num_visits > 1 {
                num_visits.to_string()
            } else if num_visits == 1 {
                visits[0].to_string()
            } else {
                ".".to_string()
            },
            _ => self.content.to_string(),
        })
    }

    fn visit(&self, direction: Direction) -> bool {
        let mut visits = self.visits.borrow_mut();
        if visits.contains(&direction) {
            false
        } else {
            visits.push(direction);
            true
        }
    }

    fn next_directions(&self, current_direction: &Direction) -> Vec<Direction> {
        match self.content {
            EmptySpace => vec![*current_direction],
            MirrorSlash => match current_direction {
                Left => vec![Down],
                Right => vec![Up],
                Up => vec![Right],
                Down => vec![Left],
            }
            MirrorBackslash => match current_direction {
                Left => vec![Up],
                Right => vec![Down],
                Up => vec![Left],
                Down => vec![Right],
            }
            SplitterVertical => match current_direction {
                Left => vec![Up, Down],
                Right => vec![Up, Down],
                Up => vec![Up],
                Down => vec![Down],
            }
            SplitterHorizontal => match current_direction {
                Left => vec![Left],
                Right => vec![Right],
                Up => vec![Left, Right],
                Down => vec![Left, Right],
            }
        }
    }

    fn is_energized(&self) -> bool {
        !self.visits.borrow().is_empty()
    }

    fn reset(&self) {
        self.visits.borrow_mut().clear();
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.content)
    }
}

#[derive(Debug, Copy, Clone)]
struct Beam {
    direction: Direction,
    position: (usize, usize),
}

struct Contraption {
    tiles: Vec<Vec<Tile>>,
    beams: RefCell<Vec<Beam>>,
}

impl Contraption {
    fn to_bounces_string(&self) -> String {
        format!("{}\n", self.tiles.iter().map(|row| {
            row.iter().map(|tile| tile.to_bounces_string()).join("")
        }).join("\n"))
    }
}

impl Contraption {
    fn progress_beams(&self) -> bool {
        let mut beams = self.beams.borrow_mut();
        //println!("{beams:?}");
        if beams.is_empty() {
            beams.push(Beam { direction: Right, position: (0, 0) });
        } else {
            *beams = beams.iter().flat_map(|beam| {
                let (current_x, current_y) = beam.position;
                let current_tile = &self.tiles[current_y][current_x];
                let next_directions = current_tile.next_directions(&beam.direction);
                next_directions.iter().filter_map(|&next_direction| {
                    ////println!("{next_direction:?} ({current_x}, {current_y})");
                    if next_direction == Up && current_y == 0 {
                        None
                    } else if next_direction == Left && current_x == 0 {
                        None
                    } else if next_direction == Down && current_y == self.tiles.len() - 1 {
                        None
                    } else if next_direction == Right && current_x == self.tiles[0].len() - 1 {
                        None
                    } else {
                        Some(Beam {
                            direction: next_direction,
                            position: match next_direction {
                                Left => (current_x - 1, current_y),
                                Right => (current_x + 1, current_y),
                                Up => (current_x, current_y - 1),
                                Down => (current_x, current_y + 1),
                            },
                        })
                    }
                }).collect::<Vec<_>>()
            }).collect();
        }

        *beams = beams.iter().filter(|beam| {
            self.tiles[beam.position.1][beam.position.0].visit(beam.direction)
        }).cloned().collect::<Vec<_>>();

        !beams.is_empty()
    }

    fn progress_to_completion(&self) {
        let mut completion_steps = 0;
        loop {
            if completion_steps % 100 == 0 {
                println!("Completion iteration {completion_steps} Number of beams {}", self.beams.borrow().len());
            }

            if self.progress_beams() == false {
                break;
            }

            completion_steps += 1
        }
    }

    fn count_energized(&self) -> u64 {
        self.tiles.iter().map(|row| {
            row.iter().map(|tile| if tile.is_energized() {
                1
            } else { 0 }).sum::<u64>()
        }).sum()
    }

    fn reset(&self, initial_beam: Beam) {
        self.tiles.iter().for_each(|row| {
            row.iter().for_each(|tile| {
                tile.reset();
            })
        });

        self.tiles[initial_beam.position.1][initial_beam.position.0].visit(initial_beam.direction);

        let mut beams = self.beams.borrow_mut();
        beams.clear();
        beams.push(initial_beam);
    }
}

impl Display for Contraption {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}\n", self.tiles.iter().map(|row| {
            row.iter().join("")
        }).join("\n"))
    }
}

fn tile_type(input: &str) -> IResult<&str, TileType> {
    let (input, tile_type) = alt((
        map(tag("."), |_| { EmptySpace }),
        map(tag("/"), |_| { MirrorSlash }),
        map(tag("\\"), |_| { MirrorBackslash }),
        map(tag("|"), |_| { SplitterVertical }),
        map(tag("-"), |_| { SplitterHorizontal }),
    ))(input)?;
    Ok((input, tile_type))
}

fn tile_row(input: &str) -> IResult<&str, Vec<Tile>> {
    let (input, tile_row) = many1(map(tile_type, |t| { Tile { content: t, visits: vec![].into() } }))(input)?;
    Ok((input, tile_row))
}

fn contraption(input: &str) -> IResult<&str, Contraption> {
    let (input, tiles) = separated_list1(newline, tile_row)(input)?;
    Ok((input, Contraption { tiles, beams: vec![].into() }))
}

fn parse_contraption(input: &str) -> Contraption {
    let (_, contraption) = contraption(input).unwrap();
    contraption
}

fn solve_part_1(input: &str) -> u64 {
    let contraption = parse_contraption(input);
    contraption.progress_to_completion();

    contraption.count_energized()
}

fn solve_part_2(input: &str) -> u64 {
    let contraption = parse_contraption(input);

    let max_left_side = (0..contraption.tiles.len()).map(|current_y| {
        if current_y % 5 == 0 {
            println!("Left side iteration: {current_y}");
        }
        contraption.reset(Beam { direction: Right, position: (0, current_y) });
        contraption.progress_to_completion();
        contraption.count_energized()
    }).max().unwrap();

    let max_right_side = (0..contraption.tiles.len()).map(|current_y| {
        if current_y % 5 == 0 {
            println!("Right side iteration: {current_y}");
        }
        contraption.reset(Beam { direction: Left, position: (contraption.tiles[0].len() - 1, current_y) });
        contraption.progress_to_completion();
        contraption.count_energized()
    }).max().unwrap();

    let max_top_side = (0..contraption.tiles[0].len()).map(|current_x| {
        if current_x % 5 == 0 {
            println!("Top side iteration: {current_x}");
        }
        contraption.reset(Beam { direction: Down, position: (current_x, 0) });
        contraption.progress_to_completion();
        contraption.count_energized()
    }).max().unwrap();

    let max_bottom_side = (0..contraption.tiles[0].len()).map(|current_x| {
        if current_x % 5 == 0 {
            println!("Bottom side iteration: {current_x}");
        }
        contraption.reset(Beam { direction: Up, position: (current_x, contraption.tiles.len() - 1) });
        contraption.progress_to_completion();
        contraption.count_energized()
    }).max().unwrap();

    max_left_side.max(max_right_side).max(max_top_side).max(max_bottom_side)
}

#[cfg(test)]
mod tests {
    use indoc::indoc;
    use crate::day_16::{parse_contraption, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_and_render_contraption() {
        let input = indoc! {r"
            .|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
        "};

        //println!("{}", input);
        assert_eq!(parse_contraption(input).to_string(), input);
    }

    #[test]
    fn should_progress_beam() {
        let input = indoc! {r"
            .|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
        "};
        let contraption = parse_contraption(input);

        contraption.progress_beams();

        let expected = indoc! {r"
            >|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
        "};

        assert_eq!(contraption.to_bounces_string(), expected);
    }

    #[test]
    fn should_behave_for_three_progressions() {
        let input = indoc! {r"
            .|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
        "};
        let contraption = parse_contraption(input);

        contraption.progress_beams();
        contraption.progress_beams();
        contraption.progress_beams();

        let expected = indoc! {r"
            >|...\....
            |v-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
        "};

        assert_eq!(contraption.to_bounces_string(), expected);
    }

    #[test]
    fn should_progress_to_completion() {
        let input = indoc! {r"
            .|...\....
            |.-.\.....
            .....|-...
            ........|.
            ..........
            .........\
            ..../.\\..
            .-.-/..|..
            .|....-|.\
            ..//.|....
        "};
        let contraption = parse_contraption(input);

        contraption.progress_to_completion();

        let expected = indoc! {r"
            >|<<<\....
            |v-.\^....
            .v...|->>>
            .v...v^.|.
            .v...v^...
            .v...v^..\
            .v../2\\..
            <->-/vv|..
            .|<<<2-|.\
            .v//.|.v..
        "};

        assert_eq!(contraption.to_bounces_string(), expected);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_16/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 46);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_16/input.txt");
        assert_eq!(solve_part_1(&input), 7236);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_16/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 51);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_16/input.txt");
        assert_eq!(solve_part_2(&input), 7521);
    }
}