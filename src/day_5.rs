use nom::bytes::complete::tag;
use nom::character::complete::{multispace1, newline, space1, u64};
use nom::IResult;
use nom::multi::separated_list1;

#[derive(PartialEq, Debug)]
struct NumberMapper {
    destination_range_start: u64,
    source_range_start: u64,
    range_length: u64,
}

impl NumberMapper {
    fn map_number(&self, input: u64) -> Option<u64> {
        if input < self.source_range_start {
            None
        } else {
            let diff_from_start = input - self.source_range_start;
            if diff_from_start > self.range_length {
                None
            } else {
                Some(diff_from_start + self.destination_range_start)
            }
        }
    }
}

#[derive(PartialEq, Debug)]
struct MultiNumberMapper {
    number_mappers: Vec<NumberMapper>,
}

impl MultiNumberMapper {
    fn map_number(&self, input: u64) -> u64 {
        self.number_mappers
            .iter()
            .filter_map(|nm| nm.map_number(input))
            .nth(0)
            .unwrap_or(input)
    }
}

#[derive(PartialEq, Debug)]

struct Almanac {
    seeds: Vec<u64>,
    seed_to_soil: MultiNumberMapper,
    soil_to_fertilizer: MultiNumberMapper,
    fertilizer_to_water: MultiNumberMapper,
    water_to_light: MultiNumberMapper,
    light_to_temperature: MultiNumberMapper,
    temperature_to_humuidity: MultiNumberMapper,
    humidity_to_location: MultiNumberMapper,
}

impl Almanac {
    fn find_lowest_location_number(&self) -> u64 {
        self.seeds.iter()
            .map(|seed| self.seed_to_soil.map_number(*seed))
            .map(|soil| self.soil_to_fertilizer.map_number(soil))
            .map(|fertilizer| self.fertilizer_to_water.map_number(fertilizer))
            .map(|water|self.water_to_light.map_number(water))
            .map(|light| self.light_to_temperature.map_number(light))
            .map(|temperature| self.temperature_to_humuidity.map_number(temperature))
            .map(|humidity| self.humidity_to_location.map_number(humidity))
            .min().unwrap()
    }

    fn find_lowest_location_number_ranges(&self) -> u64 {
        self.seeds.chunks(2).map(|chunk| {
            let start_seed = chunk[0];
            let range = chunk[1];
            start_seed..(start_seed + range)
        }).flatten().into_iter()
            .map(|seed| self.seed_to_soil.map_number(seed))
            .map(|soil| self.soil_to_fertilizer.map_number(soil))
            .map(|fertilizer| self.fertilizer_to_water.map_number(fertilizer))
            .map(|water|self.water_to_light.map_number(water))
            .map(|light| self.light_to_temperature.map_number(light))
            .map(|temperature| self.temperature_to_humuidity.map_number(temperature))
            .map(|humidity| self.humidity_to_location.map_number(humidity))
            .min().unwrap()
    }
}

fn number_mapper(input: &str) -> IResult<&str, NumberMapper> {
    let (input, destination_range_start) = u64(input)?;
    let (input, _) = space1(input)?;
    let (input, source_range_start) = u64(input)?;
    let (input, _) = space1(input)?;
    let (input, range_length) = u64(input)?;

    Ok((input, NumberMapper {
        destination_range_start,
        source_range_start,
        range_length,
    }))
}

fn multi_number_mapper(input: &str) -> IResult<&str, MultiNumberMapper> {
    let (input, number_mappers) = separated_list1(newline, number_mapper)(input)?;
    Ok((input, MultiNumberMapper { number_mappers }))
}

fn almanac(input: &str) -> IResult<&str, Almanac> {
    let (input, _) = tag("seeds:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, seeds) = separated_list1(space1, u64)(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("seed-to-soil map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, seed_to_soil) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("soil-to-fertilizer map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, soil_to_fertilizer) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("fertilizer-to-water map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, fertilizer_to_water) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("water-to-light map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, water_to_light) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("light-to-temperature map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, light_to_temperature) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("temperature-to-humidity map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, temperature_to_humuidity) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    let (input, _) = tag("humidity-to-location map:")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, humidity_to_location) = multi_number_mapper(input)?;
    let (input, _) = multispace1(input)?;

    Ok((input, Almanac{
        seeds,
        seed_to_soil,
        soil_to_fertilizer,
        fertilizer_to_water,
        water_to_light,
        light_to_temperature,
        temperature_to_humuidity,
        humidity_to_location,
    }))
}

fn solve_part_1(input: &str) -> u64 {
    let (_, almangac) = almanac(input).unwrap();
    almangac.find_lowest_location_number()
}

fn solve_part_2(input: &str) -> u64 {
    let (_, almangac) = almanac(input).unwrap();
    almangac.find_lowest_location_number_ranges()
}

#[cfg(test)]
mod tests {
    use crate::day_5::{Almanac, almanac, multi_number_mapper, MultiNumberMapper, number_mapper, NumberMapper, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_number_mapper() {
        let input = "2211745924 1281207339 39747980";
        assert_eq!(number_mapper(input), Ok(("", NumberMapper {
            destination_range_start: 2211745924,
            source_range_start: 1281207339,
            range_length: 39747980,
        })))
    }

    #[test]
    fn should_parse_multi_number_mapper() {
        let input = "2211745924 1281207339 39747980\n3832363825 4282729265 12238031";
        assert_eq!(multi_number_mapper(input), Ok(("", MultiNumberMapper {
            number_mappers: vec![NumberMapper {
                destination_range_start: 2211745924,
                source_range_start: 1281207339,
                range_length: 39747980,
            }, NumberMapper {
                destination_range_start: 3832363825,
                source_range_start: 4282729265,
                range_length: 12238031,
            }]
        })))
    }

    #[test]
    fn should_return_none_if_number_not_in_map() {
        let to_be_tested = NumberMapper {
            destination_range_start: 5,
            source_range_start: 1,
            range_length: 3,
        };

        assert_eq!(to_be_tested.map_number(50), None);
    }

    #[test]
    fn should_return_some_if_number_is_in_map() {
        let to_be_tested = NumberMapper {
            destination_range_start: 5,
            source_range_start: 1,
            range_length: 3,
        };

        assert_eq!(to_be_tested.map_number(2), Some(6));
    }

    #[test]
    fn should_return_original_number_if_not_in_any_map() {
        let to_be_tested = MultiNumberMapper{ number_mappers: vec![
            NumberMapper{
                destination_range_start: 5,
                source_range_start: 1,
                range_length: 3,
            },
            NumberMapper{
                destination_range_start: 500,
                source_range_start: 100,
                range_length: 3,
            },
        ] };

        assert_eq!(to_be_tested.map_number(50), 50);
    }

    #[test]
    fn should_return_mapped_number_if_in_any_map() {
        let to_be_tested = MultiNumberMapper{ number_mappers: vec![
            NumberMapper{
                destination_range_start: 5,
                source_range_start: 1,
                range_length: 3,
            },
            NumberMapper{
                destination_range_start: 500,
                source_range_start: 100,
                range_length: 3,
            },
        ] };

        assert_eq!(to_be_tested.map_number(2), 6);
    }

    #[test]
    fn should_parse_example() {
        let input = read_file("day_5/sample_part_1.txt");
        assert_eq!(almanac(&input), Ok(("", Almanac{
            seeds: vec![79, 14, 55, 13],
            seed_to_soil: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 50,
                    source_range_start: 98,
                    range_length: 2,
                },
                NumberMapper{
                    destination_range_start: 52,
                    source_range_start: 50,
                    range_length: 48,
                },
            ] },
            soil_to_fertilizer: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 0,
                    source_range_start: 15,
                    range_length: 37,
                },
                NumberMapper{
                    destination_range_start: 37,
                    source_range_start: 52,
                    range_length: 2,
                },
                NumberMapper{
                    destination_range_start: 39,
                    source_range_start: 0,
                    range_length: 15,
                },
            ] },
            fertilizer_to_water: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 49,
                    source_range_start: 53,
                    range_length: 8,
                },
                NumberMapper{
                    destination_range_start: 0,
                    source_range_start: 11,
                    range_length: 42,
                },
                NumberMapper{
                    destination_range_start: 42,
                    source_range_start: 0,
                    range_length: 7,
                },
                NumberMapper{
                    destination_range_start: 57,
                    source_range_start: 7,
                    range_length: 4,
                },
            ] },
            water_to_light: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 88,
                    source_range_start: 18,
                    range_length: 7,
                },
                NumberMapper{
                    destination_range_start: 18,
                    source_range_start: 25,
                    range_length: 70,
                },
            ] },
            light_to_temperature: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 45,
                    source_range_start: 77,
                    range_length: 23,
                },
                NumberMapper{
                    destination_range_start: 81,
                    source_range_start: 45,
                    range_length: 19,
                },
                NumberMapper{
                    destination_range_start: 68,
                    source_range_start: 64,
                    range_length: 13,
                },
            ] },
            temperature_to_humuidity: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 0,
                    source_range_start: 69,
                    range_length: 1,
                },
                NumberMapper{
                    destination_range_start: 1,
                    source_range_start: 0,
                    range_length: 69,
                },
            ] },
            humidity_to_location: MultiNumberMapper { number_mappers: vec![
                NumberMapper{
                    destination_range_start: 60,
                    source_range_start: 56,
                    range_length: 37,
                },
                NumberMapper{
                    destination_range_start: 56,
                    source_range_start: 93,
                    range_length: 4,
                },
            ] },
        })))
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_5/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 35);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_5/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 84470622);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_5/sample_part_1.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 46);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_5/input.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 26714516);
    }
}