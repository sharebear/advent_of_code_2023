use itertools::Itertools;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{multispace1, u64};
use nom::combinator::map;
use nom::{IResult};
use nom::multi::{count, many0, many1, separated_list1};
use regex::Regex;
use crate::day_12::Spring::{Damaged, Operational, Unknown};

#[derive(PartialEq, Debug, Copy, Clone)]
enum Spring {
    Operational,
    Damaged,
    Unknown,
}

#[derive(Debug)]
struct Row {
    condition_records: Vec<Spring>,
    contiguous_groups: Vec<u64>,
    solution_regex: Regex,
}

impl PartialEq for Row {
    fn eq(&self, other: &Self) -> bool {
        self.contiguous_groups.eq(&other.contiguous_groups) && self.condition_records.eq(&other.condition_records)
    }

    fn ne(&self, other: &Self) -> bool {
        !self.eq(other)
    }
}

fn solution(input: &str, continuous_groups: Vec<u64>) -> IResult<&str, ()> {
    let mut loopy_input = input;
    (loopy_input, _)= many0(alt((tag("."), tag("?"), )))(loopy_input)?;
    //println!("remaining input 1 {loopy_input}");

    let groups_len = continuous_groups.len();

    for (i, v) in continuous_groups.into_iter().enumerate() {
        let cnt: usize = v.try_into().unwrap();
        (loopy_input, _) = count(alt((tag("#"), tag("?"), )), cnt)(loopy_input)?;
        //println!("remaining input 2 {loopy_input}");

        if i < groups_len - 1 {
            (loopy_input, _) = many1(alt((tag("."), tag("?"), )))(loopy_input)?;
            //println!("remaining input 3 {loopy_input}");
        }

    }

    (loopy_input, _) =  many0(alt((tag("."), tag("?"), )))(loopy_input)?;
    //println!("remaining input 4 {loopy_input}");

    Ok((loopy_input, ()))
}

fn _can_be_solution_nom(test_string: &str, continuous_groups: Vec<u64>) -> bool {
    let can_be_solution = match solution(&test_string, continuous_groups) {
        Ok(_) => { true }
        Err(_x) => {
            ////println!("{:?}", x);
            false
        }
    };
    can_be_solution
}

fn can_be_solution_regex(input: &str, solution_regex: &Regex) -> bool {
    solution_regex.is_match(input)
}

impl Row {

    fn new(    condition_records: Vec<Spring>,
               contiguous_groups: Vec<u64>) -> Self {

        let start_regex = r"^[.?]*?";

        let group_count = contiguous_groups.len();

        let regex_parts : Vec<String> = contiguous_groups.iter().enumerate().map(|(i, cnt)| {
            let spacer_regex = if i < group_count - 1 {
                r"[.?]+?"
            } else { "" };
            let group_regex = format!(r"[#?]{{{cnt}}}");
            format!("{group_regex}{spacer_regex}")
        }).collect();

        let groups_regex = regex_parts.join("");

        let end_regex = r"[.?]*?$";

        let complete_regex = format!("{start_regex}{groups_regex}{end_regex}");

        let solution_regex = Regex::new(&complete_regex).unwrap();

        Row{ condition_records, contiguous_groups, solution_regex }
    }

    fn count_solutions(&self) -> u64 {
        self.count_solution_recursive(0, "")
    }

    fn count_solution_recursive(&self, position: usize, prefix: &str) -> u64 {
        let problem_length = self.condition_records.len();
        match self.condition_records[position] {
            Operational => {
                self.count_solution_operational(position, problem_length, prefix)
            }
            Damaged => {
                self.count_solution_damaged(position, problem_length, prefix)
            }
            Unknown => {
                self.count_solution_operational(position, problem_length, prefix) + self.count_solution_damaged(position, problem_length, prefix)
            }
        }
    }

    fn count_solution_operational(&self, position: usize, problem_length: usize, prefix: &str) -> u64 {
        let current_prefix = format!("{prefix}.");
        let suffix = (position..problem_length - 1).map(|_| { "?" }).join("");
        let test_string = format!("{current_prefix}{suffix}");
        let can_be_solution = can_be_solution_regex(&test_string, &self.solution_regex);
        //println!("{test_string} : {can_be_solution} : {position} : {problem_length}");
        if can_be_solution {
            if position == (problem_length - 1) {
                1
            } else {
                self.count_solution_recursive(position + 1, &current_prefix)
            }
        } else {
            0
        }
    }

    fn count_solution_damaged(&self, position: usize, problem_length: usize, prefix: &str) -> u64 {
        let current_prefix = format!("{prefix}#");
        let suffix = (position..problem_length - 1).map(|_| { "?" }).join("");
        let test_string = format!("{current_prefix}{suffix}");
        let can_be_solution = can_be_solution_regex(&test_string, &self.solution_regex);

        //println!("{test_string} : {can_be_solution} : {position} : {problem_length}");

        if can_be_solution {
            if position == (problem_length - 1) {
                1
            } else {
                self.count_solution_recursive(position + 1, &current_prefix)
            }
        } else {
            0
        }
    }

    fn fold_row(&self) -> Row {
        let folded_condition_records = (0..5).map(|n| {
            let mut records = self.condition_records.clone();
            if n < 4 {
                records.push(Unknown);
            }
            records
        }).flatten().collect();

        let folded_contiguous_groups= self.contiguous_groups.repeat(5);

        Row::new(folded_condition_records, folded_contiguous_groups)
    }
}

fn spring(input: &str) -> IResult<&str, Spring> {
    let (input, spring) = alt((
        map(tag("."), |_| { Operational }),
        map(tag("#"), |_| { Damaged }),
        map(tag("?"), |_| { Unknown }),
    ))(input)?;

    Ok((input, spring))
}

fn condition_records(input: &str) -> IResult<&str, Vec<Spring>> {
    let (input, condition_records) = many1(spring)(input)?;

    Ok((input, condition_records))
}

fn row(input: &str) -> IResult<&str, Row> {
    let (input, condition_records) = condition_records(input)?;
    let (input, _) = multispace1(input)?;
    let (input, contiguous_groups) = separated_list1(tag(","), u64)(input)?;

    Ok((input, Row::new(condition_records, contiguous_groups )))
}

fn parse_row(input: &str) -> Row {
    let (_, row) = row(input).unwrap();
    row
}

fn solve_part_1(input: &str) -> u64 {
    let line_counts: Vec<u64> = input.lines()
        .filter(|line| !line.is_empty())
        .map(parse_row).map(|row| {
        row.count_solutions()
    }).collect();
    //println!("{:?}", line_counts);
    line_counts.iter().sum()
}

fn solve_part_2(input: &str) -> u64 {
    let line_counts: Vec<u64> = input.lines()
        .filter(|line| !line.is_empty())
        .map(parse_row)
        .map(|row|row.fold_row())
        .map(|row| {
        row.count_solutions()
    }).collect();
    //println!("{:?}", line_counts);
    line_counts.iter().sum()
}

#[cfg(test)]
mod tests {
    use regex::Regex;
    use crate::day_12::{parse_row, Row, solution, solve_part_1, solve_part_2};
    use crate::day_12::Spring::{Damaged, Operational, Unknown};
    use crate::read_file;

    #[test]
    fn should_parse_row() {
        let input = "???.### 1,1,3";
        assert_eq!(parse_row(input), Row::new(
            vec![
                Unknown,
                Unknown,
                Unknown,
                Operational,
                Damaged,
                Damaged,
                Damaged,
            ],
            vec![1, 1, 3],
        ))
    }

    #[test]
    fn should_count_solutions() {
        let input = Row::new(
            vec![
                Unknown,
                Unknown,
                Unknown,
                Operational,
                Damaged,
                Damaged,
                Damaged,
            ],
            vec![1, 1, 3],
        );
        assert_eq!(input.count_solutions(), 1);
    }

    #[test]
    fn should_nom_match() {
        let input = "#.#.###";
        assert_eq!(solution(input, vec![1, 1, 3]), Ok(("", ())))
    }

    #[test]
    #[ignore] // Would have been nice with a nom solution
    fn should_nom_match_earlier() {
        let input = "#??????";
        assert_eq!(solution(input, vec![1, 1, 3]), Ok(("", ())))
    }

    #[test]
    fn should_regex_match() {
        let input = "#.#.###";
        let re = Regex::new(r"^[.?]*?[#?]{1}[.?]+?[#?]{1}[.?]+?[#?]{3}[.?]*?$").unwrap();
        assert_eq!(re.is_match(input), true);
    }

    #[test]
    fn should_regex_match_earlier() {
        let input = "#??????";
        let re = Regex::new(r"^[.?]*?[#?]{1}[.?]+?[#?]{1}[.?]+?[#?]{3}[.?]*?$").unwrap();
        assert_eq!(re.is_match(input), true);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_12/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 21);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_12/input.txt");
        assert_eq!(solve_part_1(&input), 7843);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_12/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 525152);
    }

    #[test]
    #[ignore] // Run time waaay too massive
    fn should_solve_part_2_real() {
        let input = read_file("day_12/input.txt");
        assert_eq!(solve_part_2(&input), 42); // dummy value until first run completes
    }

}