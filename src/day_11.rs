use itertools::Itertools;
use crate::day_11::ImagePixel::{EmptyCell, Galaxy};

#[derive(PartialEq, Debug)]
enum ImagePixel {
    EmptyCell,
    Galaxy {
        x: usize,
        y: usize,
    },
}

#[derive(PartialEq, Debug)]
struct Image {
    pixels: Vec<Vec<ImagePixel>>,
    expanded_rows: Vec<usize>,
    expanded_columns: Vec<usize>,
}

impl Image {
    fn new(pixels: Vec<Vec<ImagePixel>>) -> Self {
        let y_max = pixels.len();
        let x_max = pixels[0].len();

        let expanded_rows = (0..y_max).filter(|y| {
            (0..x_max).map(|x| {
                let pixel = &pixels[*y][x];
                match pixel {
                    EmptyCell => true,
                    Galaxy { .. } => false,
                }
            }).all(|is_empty| is_empty)
        }).collect();

        let expanded_columns = (0..x_max).filter(|x| {
            (0..y_max).map(|y| {
                let pixel = &pixels[y][*x];
                match pixel {
                    EmptyCell => true,
                    Galaxy { .. } => false,
                }
            }).all(|is_empty| is_empty)
        }).collect();

        Self {
            pixels,
            expanded_rows,
            expanded_columns,
        }
    }

    fn all_galaxy_pairs(&self) -> Vec<((usize, usize),(usize, usize))> {
        let all_galaxy_locations = self.all_galaxy_locations();

        all_galaxy_locations.iter()
            .combinations(2)
            .map(|combination| {
                (
                    (combination[0].0, combination[0].1),
                    (combination[1].0, combination[1].1),
                )
            }).collect()
    }

    fn all_galaxy_locations(&self) -> Vec<(usize, usize)> {
        //println!("{:?}", self.pixels.len());
        self.pixels.iter().map(|row| {
            let galaxy_locations_on_row: Vec<(usize, usize)> = row.iter().map(|pixel| {
                match pixel {
                    EmptyCell => None,
                    Galaxy { x, y } => Some((*x, *y)),
                }
            }).flatten().collect();
            galaxy_locations_on_row
        }).flatten().collect()
    }

    fn distance_between(&self, from: (usize, usize), to: (usize, usize), expansion_value: u64) -> u64 {
        //println!("from: {:?}, to: {:?}", from, to);

        let y_values = vec![from.1, to.1];

        let x_values = vec![from.0, to.0];

        let vertical_positions: Vec<(usize, usize)> = (*y_values.iter().min().unwrap()+1..=*y_values.iter().max().unwrap()).map(|y| {
            (from.0, y)
        }).collect();
        //println!("v positions {:?}", vertical_positions);
        //println!("expanded rows {:?}", self.expanded_rows);

        let vertical_weights: Vec<u64> = vertical_positions.iter().map(|position|{
            if self.expanded_rows.contains(&position.1) {
                expansion_value
            } else {
                1
            }
        }).collect();
        //println!("v weights {:?}", vertical_weights);


        let horizontal_positions: Vec<(usize, usize)> = (*x_values.iter().min().unwrap()+1..=*x_values.iter().max().unwrap()).map(|x| {
            (x, to.1)
        }).collect();
        //println!("h positions {:?}", horizontal_positions);

        let horizontal_weights: Vec<u64> = horizontal_positions.iter().map(|position|{
            if self.expanded_columns.contains(&position.0) {
                expansion_value
            } else {
                1
            }
        }).collect();
        //println!("h weights {:?}", horizontal_weights);


        horizontal_weights.iter().sum::<u64>() + vertical_weights.iter().sum::<u64>()
    }
}

fn solve_part_1(input: &str) -> u64 {
    let image = parse_image(input);
    let galaxy_pairs = image.all_galaxy_pairs();
    galaxy_pairs.iter().map(|pair| {image.distance_between(pair.0, pair.1, 2)}).sum()
}

fn solve_part_2(input: &str) -> u64 {
    let image = parse_image(input);
    let galaxy_pairs = image.all_galaxy_pairs();
    galaxy_pairs.iter().map(|pair| {image.distance_between(pair.0, pair.1, 1_000_000)}).sum()
}

fn parse_image(input: &str) -> Image {
    let image_parts = input.lines()
        .enumerate()
        .map(|(y, line)| {            line.chars().enumerate().map(|(x, char)| {
                match char {
                    '.' => EmptyCell,
                    '#' => Galaxy { x, y },
                    _ => panic!("Should not happen {:?}", char)
                }
            }).collect()
        }).collect();
    Image::new(image_parts)
}

#[cfg(test)]
mod tests {
    use crate::day_11::{Image, parse_image, solve_part_1, solve_part_2};
    use crate::day_11::ImagePixel::{EmptyCell, Galaxy};
    use crate::read_file;

    #[test]
    fn should_parse_image() {
        let input = "...#......\n.......#..\n";
        assert_eq!(parse_image(input), Image {
            pixels: vec![
                vec![
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    Galaxy { x: 3, y: 0 },
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                ],
                vec![
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    EmptyCell,
                    Galaxy { x: 7, y: 1 },
                    EmptyCell,
                    EmptyCell,
                ],
            ],
            expanded_rows: vec![],
            expanded_columns: vec![0, 1, 2, 4, 5, 6, 8, 9],
        })
    }

    #[test]
    fn should_find_expanded_rows() {
        let input = Image::new(vec![
            vec![Galaxy { x: 0, y: 0 }, Galaxy { x: 1, y: 0 }],
            vec![EmptyCell, EmptyCell],
        ]);

        assert_eq!(input.expanded_rows, vec![1]);
    }

    #[test]
    fn should_find_expanded_columns() {
        let input = Image::new(vec![
            vec![EmptyCell, Galaxy { x: 1, y: 0 }],
            vec![EmptyCell, Galaxy { x: 1, y: 1 }],
        ]);

        assert_eq!(input.expanded_columns, vec![0]);
    }

    #[test]
    fn should_find_all_galaxy_pairs() {
        let input = Image::new(vec![
            vec![EmptyCell, Galaxy { x: 1, y: 0 }, Galaxy { x: 2, y: 0 }],
            vec![EmptyCell, Galaxy { x: 1, y: 1 }],
        ]);
        assert_eq!(input.all_galaxy_pairs(), vec![
            ((1, 0), (2, 0),),
            ((1, 0), (1, 1),),
            ((2, 0), (1, 1),),
        ]);
    }

    #[test]
    fn should_find_correct_number_of_pairs_in_sample() {
        let input = read_file("day_11/sample_part_1.txt");
        let image = parse_image(&input);
        assert_eq!(image.all_galaxy_pairs().len(), 36);
    }

    #[test]
    fn should_find_all_galaxy_locations_in_sample() {
        let input = read_file("day_11/sample_part_1.txt");
        let image = parse_image(&input);

        println!("{:?}", image);

        assert_eq!(image.all_galaxy_locations(), vec![
            (3, 0),
            (7, 1),
            (0, 2),
            (6, 4),
            (1, 5),
            (9,6),
            (7,8),
            (0,9),
            (4,9),
        ]);
    }

    #[test]
    fn should_find_distance_between_example_pair() {
        let input = read_file("day_11/sample_part_1.txt");
        let image = parse_image(&input);
        let all_pairs = image.all_galaxy_pairs();
        let example_pair = all_pairs[29];
        assert_eq!(image.distance_between(example_pair.0, example_pair.1, 2), 9);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_11/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 374);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_11/input.txt");
        assert_eq!(solve_part_1(&input), 10490062);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_11/input.txt");
        assert_eq!(solve_part_2(&input), 382979724122);
    }
}