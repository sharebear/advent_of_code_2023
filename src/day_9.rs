use nom::character::complete::{i64, multispace1};
use nom::IResult;
use nom::multi::{separated_list1};

#[derive(PartialEq, Debug)]
struct History {
    values: Vec<i64>,
}

#[derive(PartialEq, Debug, Clone)]
struct Differences {
    values: Vec<i64>,
}

impl History {
    fn generate_diff(&self) -> Differences {
        let values = self.values.windows(2).map(|pair| {
            pair[1] - pair[0]
        }).collect();
        Differences{ values }
    }

    fn calculate_next_value(&self) -> i64 {
        let mut diffs = Vec::new();
        let mut current_diff = self.generate_diff();
        diffs.push(current_diff.clone());

        while !current_diff.is_last_diff() {
            current_diff = current_diff.generate_diff();
            diffs.push(current_diff.clone());
        }

        let mut last_values: Vec<i64> = diffs.iter().map(|diff| {
            diff.last_value()
        }).collect();

        last_values.remove(last_values.len() - 1);

        last_values.reverse();

        last_values.push(*self.values.last().unwrap());

        let result = last_values.iter().fold(0i64, |acc, next| {
            acc + next
        });


        println!("{:?}", last_values);
        println!("{:?}", result);

        result
    }

    fn calculate_previous_value(&self) -> i64 {
        let mut diffs = Vec::new();
        let mut current_diff = self.generate_diff();
        diffs.push(current_diff.clone());

        while !current_diff.is_last_diff() {
            current_diff = current_diff.generate_diff();
            diffs.push(current_diff.clone());
        }

        let mut first_values: Vec<i64> = diffs.iter().map(|diff| {
            diff.first_value()
        }).collect();

        first_values.remove(first_values.len() - 1);

        first_values.reverse();

        first_values.push(*self.values.first().unwrap());

        let result = first_values.iter().fold(0i64, |acc, next| {
            next - acc
        });


        println!("{:?}", first_values);
        println!("{:?}", result);

        result
    }
}

impl Differences {
    fn is_last_diff(&self) -> bool {
        self.values.iter().all(|value| { value == &0 })
    }

    fn generate_diff(&self) -> Differences {
        let values = self.values.windows(2).map(|pair| {
            pair[1] - pair[0]
        }).collect();
        Differences{ values }
    }

    fn last_value(&self) -> i64 {
        *self.values.last().unwrap()
    }

    fn first_value(&self) -> i64 {
        *self.values.first().unwrap()
    }
}

fn history(input: &str) -> IResult<&str, History> {
    let( input, values) = separated_list1(multispace1, i64)(input)?;
    Ok((input, History{ values }))
}

fn parse_history(input: &str) -> History {
    let (_, history) = history(input).unwrap();
    history
}

fn solve_part_1(input: &str) -> i64 {
    let oasis_report:Vec<History> = input.lines().map(parse_history).collect();
    oasis_report.iter().map(|history| {
        history.calculate_next_value()
    }).sum()
}

fn solve_part_2(input: &str) -> i64 {
    let oasis_report:Vec<History> = input.lines().map(parse_history).collect();
    oasis_report.iter().map(|history| {
        history.calculate_previous_value()
    }).sum()
}

#[cfg(test)]
mod tests {
    use crate::day_9::{Differences, History, history, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_history() {
        let input = "10  13  16  21  30  45";
        assert_eq!(history(input), Ok(("", History{ values: vec![10, 13, 16, 21, 30, 45] })));
    }

    #[test]
    fn should_generate_diff_for_history() {
        let input = History{ values: vec![0, 3, 6, 9, 12, 15] };
        assert_eq!(input.generate_diff(), Differences{ values: vec![3, 3, 3, 3, 3] })
    }

    #[test]
    fn should_be_last_diff_if_all_values_are_0() {
        let input = Differences{ values: vec![0, 0, 0, 0, 0] };
        assert_eq!(input.is_last_diff(), true)
    }

    #[test]
    fn should_find_next_value() {
        let input = History{ values: vec![0, 3, 6, 9, 12, 15] };
        assert_eq!(input.calculate_next_value(), 18);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_9/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 114);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_9/input.txt");
        assert_eq!(solve_part_1(&input), 1898776583);
    }

    #[test]
    fn should_find_previous_value() {
        let input = History{ values: vec![10, 13, 16, 21, 30, 45] };
        assert_eq!(input.calculate_previous_value(), 5);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_9/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 2);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_9/input.txt");
        assert_eq!(solve_part_2(&input), 1100);
    }
}