use std::fmt::{Debug};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::newline;
use nom::combinator::map;
use nom::IResult;
use nom::multi::{many0, many1, separated_list1};
use crate::day_13::PatternElement::{Ash, Rocks};

#[derive(PartialEq, Debug, Clone, Copy)]
enum PatternElement {
    Ash,
    Rocks,
}

struct Pattern {
    elements: Vec<Vec<PatternElement>>,
}

impl Pattern {
    fn find_vertical_folds(&self) -> Vec<usize> {
        let mut fold_candidates: Vec<usize> = (0..self.elements[0].len()-1).collect();
        for row_index in 0..self.elements.len() {
            fold_candidates = find_folding_candidates(&self.elements[row_index], fold_candidates);
        }
        fold_candidates
    }

    fn find_horizontal_folds(&self) -> Vec<usize> {
        let mut fold_candidates : Vec<usize> = (0..self.elements.len()-1).collect();
        for column_index in 0..self.elements[0].len() {
            let column_values = self.values_for_column(column_index);
            fold_candidates = find_folding_candidates(&column_values, fold_candidates);
        }
        fold_candidates
    }

    fn values_for_column(&self, column_index:usize) -> Vec<PatternElement> {
        (0..self.elements.len()).map(|row_index|self.elements[row_index][column_index]).collect()
    }

    fn summarize(&self) -> Summary {
        let vertical_folds = self.find_vertical_folds();
        let horizontal_folds = self.find_horizontal_folds();

        Summary { vertical_folds, horizontal_folds }
    }

    fn find_smudge_summary(&self) -> Summary {
        let original_summary = self.summarize();

        for y in 0.. self.elements.len() {
            for x in 0..self.elements[0].len() {
                let smudged = self.smudge(x, y);
                let new_summary = smudged.summarize();
                let new_summary = new_summary.remove(&original_summary);
                //println!("{new_summary:?}");
                if new_summary.has_exactly_one_fold() {
                    return new_summary;
                }
            }
        }
        panic!("Did not find smudged solution!");
    }

    fn smudge(&self, x: usize, y:usize) -> Pattern {
        let mut new_elements = self.elements.clone();
        new_elements[y][x] = match self.elements[y][x] {
            Ash => Rocks,
            Rocks => Ash,
        };
        Pattern{ elements: new_elements }
    }
}

#[derive(PartialEq, Debug)]
struct Summary {
    vertical_folds: Vec<usize>,
    horizontal_folds: Vec<usize>,
}

impl Summary {
    fn has_exactly_one_fold(&self) -> bool {
        (self.vertical_folds.len() == 1) ^ (self.horizontal_folds.len() == 1)
    }

    fn value(&self) -> usize {
        let vertical_summary:usize = self.vertical_folds.iter().map(|x|x+1).sum();

        let horizontal_summary:usize = self.horizontal_folds.iter().map(|x|x+1).sum::<usize>() * 100;

        let summary = vertical_summary + horizontal_summary;
        //println!("{vertical_folds:?}, {vertical_summary:?}, {horizontal_folds:?}, {horizontal_summary:?}, {summary:?}");
        summary
    }

    fn remove(&self, original: &Summary) -> Summary {
        Summary{
            vertical_folds: self.vertical_folds.clone() // FIXME: Not sure this clone is so wise..
                .into_iter()
                .filter(|item|!original.vertical_folds.contains(item))
                .collect(),
            horizontal_folds: self.horizontal_folds.clone()
                .into_iter()
                .filter(|item|!original.horizontal_folds.contains(item))
                .collect(),
        }
    }
}

fn pattern_element(input: &str) -> IResult<&str, PatternElement> {
    let (input, pattern_elements) = alt((
        map(tag("."), |_| { Ash }),
        map(tag("#"), |_| { Rocks }),
    ))(input)?;
    Ok((input, pattern_elements))
}

fn row(input: &str) -> IResult<&str, Vec<PatternElement>> {
    let (input, row) = many1(pattern_element)(input)?;
    let (input, _) = newline(input)?;
    Ok((input, row))
}

fn pattern(input: &str) -> IResult<&str, Pattern> {
    let (input, elements) = many1(row)(input)?;
    Ok((input, Pattern { elements }))
}

fn patterns(input: &str) -> IResult<&str, Vec<Pattern>> {
    let( input, patterns) = separated_list1(newline, pattern)(input)?;
    let (input, _) = many0(newline)(input)?;
    Ok ( (input, patterns))
}

fn parse_patterns(input: &str) -> Vec<Pattern> {
    let (_remains, patterns) = patterns(input).unwrap();
    //println!("{remains}");
    patterns
}

fn can_fold_at(input: &Vec<PatternElement>, index: usize) -> bool {
    let distance_to_left_side = index + 1;
    let distance_to_right_side = input.len() - index - 1;
    let fold_size = distance_to_left_side.min(distance_to_right_side);

    for i in 0..fold_size {
        let left_pattern_element = &input[index-i];
        let right_pattern_element = &input[index+i+1];
        if !left_pattern_element.eq(&right_pattern_element) {
            return false;
        }
    }
    true
}

fn find_folding_candidates(input: &Vec<PatternElement>, folding_candidates: Vec<usize>) -> Vec<usize> {
    folding_candidates.into_iter().filter(|candidate|can_fold_at(input, *candidate)).collect()
}

fn solve_part_1(input: &str) -> usize {
    let patterns = parse_patterns(input);
    patterns.iter().map(|pattern|pattern.summarize().value()).sum()
}

fn solve_part_2(input: &str) -> usize {
    let patterns = parse_patterns(input);
    patterns.iter().map(|pattern|pattern.find_smudge_summary().value()).sum()
}

#[cfg(test)]
mod tests {
    use crate::day_13::{can_fold_at, find_folding_candidates, Pattern, solve_part_1, solve_part_2};
    use crate::day_13::PatternElement::{Ash, Rocks};
    use crate::read_file;

    #[test]
    fn should_not_be_foldable_at_first_index_of_first_example() {
        // #.##..##.
        let input =  &vec![Rocks, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Ash];
        assert_eq!(can_fold_at(input, 0), false);
    }

    #[test]
    fn should_be_foldable_at_fourth_index_of_first_example() {
        // #.##..##.
        let input =  &vec![Rocks, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Ash];
        assert_eq!(can_fold_at(input, 4), true);
    }

    #[test]
    fn should_find_folding_candidates_for_row() {
        // #.##..##.
        let input =  vec![Rocks, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Ash];
        let initial_candidates: Vec<usize> = (0..input.len()-1).collect();
        assert_eq!(find_folding_candidates(&input, initial_candidates), vec![4, 6]);
    }

    #[test]
    fn should_find_vertical_folding_candidates_for_pattern() {
        let input =  Pattern {
            elements: vec![
                // #.##..##.
                vec![Rocks, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Ash],
                // ..#.##.#.
                vec![Ash, Ash, Rocks, Ash, Rocks, Rocks, Ash, Rocks, Ash],
                // ##......#
                vec![Rocks, Rocks, Ash, Ash, Ash, Ash, Ash, Ash, Rocks],
                // ##......#
                vec![Rocks, Rocks, Ash, Ash, Ash, Ash, Ash, Ash, Rocks],
                // ..#.##.#.
                vec![Ash, Ash, Rocks, Ash, Rocks, Rocks, Ash, Rocks, Ash],
                // ..##..##.
                vec![Ash, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Ash],
                // #.#.##.#.
                vec![Rocks, Ash, Rocks, Ash, Rocks, Rocks, Ash, Rocks, Ash],
            ],
        };
        assert_eq!(input.find_vertical_folds(), vec![4]);
    }

    #[test]
    fn should_find_horizontal_folding_candidates_for_pattern() {
        let input =  Pattern {
            elements: vec![
                // #...##..#
                vec![Rocks, Ash, Ash, Ash, Rocks, Rocks, Ash, Ash, Rocks],
                // #....#..#
                vec![Rocks, Ash, Ash, Ash, Ash, Rocks, Ash, Ash, Rocks],
                // ..##..###
                vec![Ash, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Rocks],
                // #####.##.
                vec![Rocks, Rocks, Rocks, Rocks, Rocks, Ash, Rocks, Rocks, Ash],
                // #####.##.
                vec![Rocks, Rocks, Rocks, Rocks, Rocks, Ash, Rocks, Rocks, Ash],
                // ..##..###
                vec![Ash, Ash, Rocks, Rocks, Ash, Ash, Rocks, Rocks, Rocks],
                // #....#..#
                vec![Rocks, Ash, Ash, Ash, Ash, Rocks, Ash, Ash, Rocks],
            ],
        };
        assert_eq!(input.find_horizontal_folds(), vec![3]);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_13/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 405);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_13/input.txt");
        assert_eq!(solve_part_1(&input), 31877);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_13/sample_part_1.txt");
        assert_eq!(solve_part_2(&input), 400);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_13/input.txt");
        assert_eq!(solve_part_2(&input), 42996);
    }

}
