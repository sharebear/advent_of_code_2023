use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{anychar, u32};
use nom::IResult;
use nom::multi::many1;
use nom_locate::{LocatedSpan, position};

#[derive(PartialEq, Debug, Clone)]
enum Stuff {
    Number { value: i32, location: Vec<(i32, i32)>, border: Vec<(i32, i32)> },
    Star { location: (i32, i32), border: Vec<(i32, i32)> },
    Symbol { location: (i32, i32) },
    Periods,
}

type Span<'a> = LocatedSpan<&'a str>;

fn calculate_border_locations(line: i32, start_column: i32, end_column: i32) -> Vec<(i32, i32)> {
    let mut vec = Vec::new();
    for x in line - 1..=line + 1 {
        for y in start_column - 1..end_column + 1 {
            vec.push((x, y));
        }
    }
    vec
}

fn calculate_number_location(line: i32, start_column: i32, end_column: i32) -> Vec<(i32, i32)> {
    let mut vec = Vec::new();
    for y in start_column..end_column {
        vec.push((line, y));
    }
    vec
}

fn number(input: Span) -> IResult<Span, Stuff> {
    let (input, start_position) = position(input)?;
    let (input, value) = u32(input)?;
    let (input, end_position) = position(input)?;

    let location = calculate_number_location(
        start_position.location_line() as i32,
        start_position.get_column() as i32,
        end_position.get_column() as i32,
    );

    let border = calculate_border_locations(
        start_position.location_line() as i32,
        start_position.get_column() as i32,
        end_position.get_column() as i32,
    );

    Ok((input, Stuff::Number {
        value: value.try_into().unwrap(),
        location,
        border,
    }))
}

fn periods(input: Span) -> IResult<Span, Stuff> {
    let (input, _) = many1(alt((tag("."), tag("\n"))))(input)?;
    Ok((input, Stuff::Periods))
}

fn symbol(input: Span) -> IResult<Span, Stuff> {
    let (input, start_position) = position(input)?;
    let (input, _) = anychar(input)?; // Note this must come last, would match both digits and dots
    Ok((input, Stuff::Symbol { location: (start_position.location_line() as i32, start_position.get_column() as i32) }))
}

fn star(input: Span) -> IResult<Span, Stuff> {
    let (input, start_position) = position(input)?;
    let (input, _) = tag("*")(input)?; // Note this must come last, would match both digits and dots
    let (input, end_position) = position(input)?;

    let border = calculate_border_locations(
        start_position.location_line() as i32,
        start_position.get_column() as i32,
        end_position.get_column() as i32,
    );

    Ok((input, Stuff::Star {
        location: (start_position.location_line() as i32, start_position.get_column() as i32),
        border,
    }))
}

fn schematic(input: Span) -> IResult<Span, Vec<Stuff>> {
    let (input, result) = many1(alt((number, periods, star, symbol)))(input)?;
    Ok((input, result))
}

fn parse_schematic(input: &str) -> Vec<Stuff> {
    let spanny = Span::new(input);
    let (_, result) = schematic(spanny).unwrap();
    result
}

fn solve_part_1(input: &str) -> i32 {
    let flat_stuff = parse_schematic(input);

    let symbol_locations: Vec<(i32, i32)> = flat_stuff.clone().into_iter().filter_map(|thing| {
        match thing {
            Stuff::Number { .. } => None,
            Stuff::Symbol { location } => Some(location),
            Stuff::Periods => None,
            Stuff::Star { location, .. } => Some(location),
        }
    }).collect();

    println!("{:?}", symbol_locations);


    let part_numbers: Vec<i32> = flat_stuff.into_iter().filter_map(|thing| {
        match thing {
            Stuff::Number { value, border, .. } => if border.iter().any(|border_location| {
                symbol_locations.contains(border_location)
            }) {
                Some(value)
            } else {
                None
            },
            Stuff::Symbol { .. } => None,
            Stuff::Star { .. } => None,
            Stuff::Periods => None,
        }
    }).collect();

    println!("{:?}", part_numbers);

    part_numbers.iter().sum()
}

fn find_adjoining_numbers(star_border: Vec<(i32, i32)>, stuff: Vec<Stuff>) -> Vec<i32> {
    let adjoining_numbers: Vec<i32> = stuff.into_iter().filter_map(|thing| {
        match thing {
            Stuff::Number { value, location, .. } => {
                if location.iter().any(|position| star_border.contains(&position)) {
                    Some(value)
                } else {
                    None
                }
            },
            Stuff::Star { .. } => None,
            Stuff::Symbol { .. } => None,
            Stuff::Periods => None,
        }
    }).collect();
    adjoining_numbers
}

fn solve_part_2(input: &str) -> i32 {
    let flat_stuff = parse_schematic(input);

    flat_stuff.clone().into_iter().filter_map(|thing| {
        match thing {
            Stuff::Number { .. } => None::<i32>,
            Stuff::Symbol { .. } => None,
            Stuff::Periods => None,
            Stuff::Star { border, .. } => {
                let adjoining_numbers = find_adjoining_numbers(border, flat_stuff.clone());
                if adjoining_numbers.len() == 2 {
                    Some(adjoining_numbers.iter().product())
                } else {
                    None
                }
            }
        }
    }).sum()
}

#[cfg(test)]
mod tests {
    use crate::day_3::{schematic, solve_part_1, solve_part_2, Span};
    use crate::day_3::Stuff::{Number, Periods, Star, Symbol};
    use crate::read_file;

    #[test]
    fn should_parse_line_with_numbers() {
        let input = Span::new("467..114..");
        let (_, result) = schematic(input).unwrap();
        assert_eq!(result, vec![
            Number {
                value: 467,
                location: vec![
                    (1, 1),
                    (1, 2),
                    (1, 3),
                ],
                border: vec![
                    // I know line -1 doesn't exist, but it avoids me having to special case the first and last rows
                    (0, 0),
                    (0, 1),
                    (0, 2),
                    (0, 3),
                    (0, 4),
                    (1, 0),
                    (1, 1),// I also know these three locations can't contain a symbol but again, quicker to avoid the special casing
                    (1, 2),
                    (1, 3),
                    (1, 4),
                    (2, 0),
                    (2, 1),
                    (2, 2),
                    (2, 3),
                    (2, 4),
                ],
            },
            Periods,
            Number {
                value: 114,
                location: vec![
                    (1, 6),
                    (1, 7),
                    (1, 8),
                ],
                border: vec![
                    (0, 5),
                    (0, 6),
                    (0, 7),
                    (0, 8),
                    (0, 9),
                    (1, 5),
                    (1, 6),
                    (1, 7),
                    (1, 8),
                    (1, 9),
                    (2, 5),
                    (2, 6),
                    (2, 7),
                    (2, 8),
                    (2, 9),
                ],
            },
            Periods,
        ])
    }

    #[test]
    fn should_parse_line_with_symbols() {
        let input = Span::new("...$.*....");
        let (_, result) = schematic(input).unwrap();
        assert_eq!(result, vec![
            Periods,
            Symbol { location: (1, 4) },
            Periods,
            Star {
                location: (1, 6),
                border: vec![
                    (0, 5),
                    (0, 6),
                    (0, 7),
                    (1, 5),
                    (1, 6),
                    (1, 7),
                    (2, 5),
                    (2, 6),
                    (2, 7),
                ],
            },
            Periods,
        ])
    }

    #[test]
    fn should_parse_line_with_dash_as_symbol_not_negation() {
        let input = Span::new(".....-939....");
        let (_, result) = schematic(input).unwrap();
        assert_eq!(result, vec![
            Periods,
            Symbol { location: (1, 6) },
            Number {
                value: 939,
                location: vec![
                    (1, 7),
                    (1, 8),
                    (1, 9),
                ],
                border: vec![
                    (0, 6),
                    (0, 7),
                    (0, 8),
                    (0, 9),
                    (0, 10),
                    (1, 6),
                    (1, 7),
                    (1, 8),
                    (1, 9),
                    (1, 10),
                    (2, 6),
                    (2, 7),
                    (2, 8),
                    (2, 9),
                    (2, 10),
                ],
            },
            Periods,
        ])
    }

    #[test]
    fn should_parse_multi_line_with_symbols() {
        let input = Span::new("...$......\n......#...");
        let (_, result) = schematic(input).unwrap();
        assert_eq!(result, vec![
            Periods,
            Symbol { location: (1, 4) },
            Periods,
            Symbol { location: (2, 7) },
            Periods,
        ])
    }

    #[test]
    fn should_parse_line_with_numbers_and_symbols() {
        let input = Span::new("617*......");
        let (_, result) = schematic(input).unwrap();
        assert_eq!(result, vec![
            Number {
                value: 617,
                location: vec![
                    (1, 1),
                    (1, 2),
                    (1, 3),
                ],
                border: vec![
                    (0, 0),
                    (0, 1),
                    (0, 2),
                    (0, 3),
                    (0, 4),
                    (1, 0),
                    (1, 1),
                    (1, 2),
                    (1, 3),
                    (1, 4),
                    (2, 0),
                    (2, 1),
                    (2, 2),
                    (2, 3),
                    (2, 4),
                ],
            },
            Star {
                location: (1, 4),
                border: vec![
                    (0, 3),
                    (0, 4),
                    (0, 5),
                    (1, 3),
                    (1, 4),
                    (1, 5),
                    (2, 3),
                    (2, 4),
                    (2, 5),
                ],
            },
            Periods,
        ])
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_3/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 4361);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_3/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 556367);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_3/sample_part_1.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 467835);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_3/input.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 89471771);
    }
}