use std::collections::HashMap;
use nom::branch::alt;
use nom::bytes::complete::{tag, take_until1};
use nom::character::complete::{multispace0, multispace1, newline};
use nom::combinator::map;
use nom::IResult;
use nom::multi::{many1, separated_list1};
use num::integer::lcm;
use crate::day_8::Instruction::{Left, Right};

#[derive(PartialEq, Debug)]
enum Instruction {
    Left,
    Right,
}

#[derive(PartialEq, Debug)]
struct Document<'a> {
    instructions: Vec<Instruction>,
    network: HashMap<&'a str, (&'a str, &'a str)>,
}

impl Document<'_> {
    fn find_start_locations(&self) -> Vec<&str> {
        self.network
            .keys()
            .filter(|location| location.ends_with("A"))
            .map(|wat| *wat)// Rust n00bing here, there must be a better way to do this
            .collect()
    }
}

fn instruction(input: &str) -> IResult<&str, Instruction> {
    let (input, direction) = alt((
        map(tag("L"), |_| {Left}),
        map(tag("R"), |_| {Right}),
    ))(input)?;
    Ok((input, direction))
}

fn instruction_list(input: &str) -> IResult<&str, Vec<Instruction>> {
    let (input, direction_list) = many1(instruction)(input)?;
    Ok((input, direction_list))
}

fn next_nodes(input: &str) -> IResult<&str, (&str, &str)> {
    let (input, _) = tag("(")(input)?;
    let (input, left_node) = take_until1(", ")(input)?;
    let (input, _) = tag(", ")(input)?;
    let (input, right_node) = take_until1(")")(input)?;
    let (input, _) = tag(")")(input)?;
    Ok((input, (left_node, right_node)))
}

fn network_line(input: &str) -> IResult<&str, (&str, (&str, &str))> {
    let (input, current_node) = take_until1(" = ")(input)?;
    let (input, _) = tag(" = ")(input)?;
    let (input, next_nodes) = next_nodes(input)?;
    Ok((input, (current_node, next_nodes)))
}

fn document(input: &str) -> IResult<&str, Document> {
    let (input, instructions) = instruction_list(input)?;
    let (input, _) = multispace1(input)?;
    let (input, lines) = separated_list1(newline, network_line)(input)?;
    let (input, _) = multispace0(input)?;
    let network = lines.into_iter().collect();
    Ok((input, Document{ instructions, network }))
}

fn solve_part_1(input: &str) -> u64 {
    let (_, document) = document(input).unwrap();
    let mut  move_counter = 0u64;
    let mut current_location = "AAA";
    'outer: loop {
        for next_move in &document.instructions {
            move_counter = move_counter + 1;
            let next_moves = document.network.get(current_location).unwrap();
            let next_location = match next_move {
                Left => {next_moves.0}
                Right => {next_moves.1}
            };
            //println!("current: {:?}, direction: {:?}, next: {:?}", current_location, next_location, next_move);
            current_location = next_location;
            if current_location == "ZZZ" {
                break 'outer
            }
        }
    }

    move_counter
}

fn solve_part_2(input: &str) -> u64 {
    let (_, document) = document(input).unwrap();
    let start_locations = document.find_start_locations();

    let distance_to_first_exit: Vec<u64> = start_locations.iter().map(|start_location| {
        let mut current_location = *start_location;
        let mut move_counter = 0u64;

        'outer: loop {
            for next_move in &document.instructions {
                move_counter = move_counter + 1;
                let next_moves = document.network.get(current_location).unwrap();
                let next_location = match next_move {
                    Left => {next_moves.0}
                    Right => {next_moves.1}
                };
                //println!("current: {:?}, direction: {:?}, next: {:?}", current_location, next_location, next_move);
                current_location = next_location;
                if current_location.ends_with("Z") {
                    break 'outer
                }
            }
        }

        move_counter
    }).collect();

    distance_to_first_exit.iter().fold(1, |acc, e| { lcm(acc, *e) })
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use crate::day_8::Instruction::{Left, Right};
    use crate::day_8::{document, Document, instruction_list, network_line, next_nodes, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_instruction_list() {
        let input = "LLR";
        assert_eq!(instruction_list(input), Ok(("", vec![Left, Left, Right])));
    }

    #[test]
    fn should_parse_next_nodes() {
        let input = "(MTC, PDN)";
        assert_eq!(next_nodes(input), Ok(("", ("MTC", "PDN"))));
    }

    #[test]
    fn should_parse_network_line() {
        let input = "SRR = (MTC, PDN)";
        assert_eq!(network_line(input), Ok(("", ("SRR", ("MTC", "PDN")))));
    }

    #[test]
    fn should_parse_example() {
        let input = read_file("day_8/sample_part_1.txt");
        assert_eq!(document(&input), Ok(("", Document{
            instructions: vec![Left, Left, Right],
            network: HashMap::from([
                ("AAA", ("BBB", "BBB")),
                ("BBB", ("AAA", "ZZZ")),
                ("ZZZ", ("ZZZ", "ZZZ")),
            ]),
        })));
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_8/sample_part_1.txt");
        assert_eq!(solve_part_1(&input), 6);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_8/input.txt");
        assert_eq!(solve_part_1(&input), 21409);
    }

    #[test]
    fn should_find_start_locations() {
        let input = Document{ instructions: vec![], network: HashMap::from([
            ("11A", ("11B", "XXX")),
            ("22A", ("11B", "XXX")),
            ("11B", ("11B", "XXX")),
        ]) };
        let mut start_locations = input.find_start_locations();
        start_locations.sort();
        assert_eq!(start_locations, vec!["11A", "22A"]);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_8/sample_part_2.txt");
        assert_eq!(solve_part_2(&input), 6);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_8/input.txt");
        let result = solve_part_2(&input);
        println!("{:?}", result);
        assert_eq!(result, 21165830176709u64);
    }
}