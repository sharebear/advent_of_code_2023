use std::cell::Cell;
use nom::bytes::complete::{tag, take_until1};
use nom::character::complete::{multispace1, u32};
use nom::IResult;
use nom::multi::{many0, many1, separated_list1};

#[derive(PartialEq, Debug)]
struct Card {
    number: u32,
    winning_numbers: Vec<u32>,
    my_numbers: Vec<u32>,
    count: Cell<u32>,
}

impl Card {
    fn calculate_score(&self) -> u32 {
        let mut score: u32 = 0;
        for winner in self.winning_numbers.iter() {
            if self.my_numbers.contains(winner) {
                if score == 0 {
                    score = 1;
                } else {
                    score = score * 2
                }
            }
        }
        score
    }

    fn matching_number_count(&self) -> u32 {
        self.winning_numbers.iter().filter(|n|self.my_numbers.contains(n)).count() as u32
    }

    fn increment_count_by(&self, increment: u32) -> () {
        let current_count = self.count.get();
        self.count.replace(current_count + increment);
    }
}

fn number_list(input: &str) -> IResult<&str, Vec<u32>> {
    let (input, _) = many0(tag(" "))(input)?;
    let (input, list) = separated_list1(many1(tag(" ")), u32)(input)?;
    let (input, _) = many0(tag(" "))(input)?;
    Ok((input, list))
}

fn card(input: &str) -> IResult<&str, Card> {
    let (input, _) = tag("Card")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, number) = u32(input)?;
    let (input, _) = tag(":")(input)?;
    let (input, winning_numbers_string) = take_until1("|")(input)?;
    let (input, _) = tag("|")(input)?;
    let (input, my_numbers) = number_list(input)?;

    let (_, winning_numbers) = number_list(winning_numbers_string)?;

    Ok((input, Card {
        number,
        winning_numbers,
        my_numbers,
        count: Cell::new(1),
    }))
}

fn parse_card(input: &str) -> Card {
    let (_, card) = card(input).unwrap();
    card
}

fn solve_part_1(input: &str) -> u32 {
    input.lines().into_iter()
        .map(parse_card)
        .map(|card| card.calculate_score())
        .sum()
}

fn solve_part_2(input: &str) -> u32 {
    let card_list: Vec<Card> = input.lines().into_iter()
        .map(parse_card)
        .collect();

    for (idx, card) in card_list.iter().enumerate() {
        let copy_cards = card.matching_number_count();
        let number_of_copies = card.count.get();

        for i in 1..=copy_cards {
            let copy_idx = idx + i as usize;
            card_list[copy_idx].increment_count_by(number_of_copies);

        }
    }

    card_list.iter().map(|card| card.count.get()).sum()
}

#[cfg(test)]
mod tests {
    use std::cell::Cell;
    use crate::day_4::{Card, number_list, parse_card, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_parse_number_list() {
        let input = "41 48 83 86 17";
        assert_eq!(number_list(input), Ok(("", vec![41, 48, 83, 86, 17])));
    }

    #[test]
    fn should_parse_number_list_with_surrounding_space() {
        let input = "41 48 83 86 17 ";
        assert_eq!(number_list(input), Ok(("", vec![41, 48, 83, 86, 17])));
    }

    #[test]
    fn should_parse_number_list_with_single_digit_number() {
        let input = "41  8 83 86 17 ";
        assert_eq!(number_list(input), Ok(("", vec![41, 8, 83, 86, 17])));
    }

    #[test]
    fn should_parse_card() {
        let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";
        assert_eq!(parse_card(input), Card {
            number: 1,
            winning_numbers: vec![
                41,
                48,
                83,
                86,
                17,
            ],
            my_numbers: vec![
                83,
                86,
                6,
                31,
                17,
                9,
                48,
                53,
            ],
            count: Cell::new(1)
        })
    }

    #[test]
    fn should_calculate_score() {
        let input = Card {
            number: 1,
            winning_numbers: vec![
                41,
                48,
                83,
                86,
                17,
            ],
            my_numbers: vec![
                83,
                86,
                6,
                31,
                17,
                9,
                48,
                53,
            ],
            count: Cell::new(42),
        };

        assert_eq!(input.calculate_score(), 8);
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_4/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 13);
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_4/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 21568);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_4/sample_part_1.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 30);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_4/input.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 11827296);
    }
}