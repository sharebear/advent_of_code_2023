use float_eq::float_eq;
use nom::bytes::complete::tag;
use nom::character::complete::{i64, multispace0, multispace1};
use nom::IResult;
use nom::multi::{separated_list1};
use roots::{find_roots_quadratic, Roots};

fn fudge_numbers(lower :f64, higher:f64) -> (i64, i64) {
    println!("{:?}, {:?}", lower, higher);
    println!("{:?}, {:?}", lower.ceil(), higher.floor());
    let lower_result = if float_eq!(lower, lower.ceil(), abs <= 0.1) {
        lower.ceil() as i64 + 1
    } else {
        lower.ceil() as i64
    };
    let higher_result = if float_eq!(higher, higher.floor(), abs <= 0.1) {
        higher.floor() as i64 -1
    } else {
        higher.floor() as i64
    };
    println!("{:?}, {:?}", lower_result, higher_result);
    (lower_result, higher_result)
}

fn find_whole_roots(time: i64, distance: i64) -> (i64, i64) {
    println!("time: {:?}, distance: {:?}", time, distance);
    let result = find_roots_quadratic(-1f64, time as f64, (1 - distance) as f64);
    match result {
        Roots::No(_) => panic!("Should not happen"),
        Roots::One(_) => panic!("Should not happen"),
        Roots::Two(r) => fudge_numbers(r[0], r[1]),
        Roots::Three(_) => panic!("Should not happen"),
        Roots::Four(_) => panic!("Should not happen"),
    }
}

fn find_number_of_ways(time: i64, distance: i64) -> i64 {
    let roots = find_whole_roots(time, distance);
    roots.1 - roots.0 + 1
}

fn document(input: &str) -> IResult<&str, Vec<(i64, i64)>> {
    let (input, _) = tag("Time:")(input)?;
    let (input, _) = multispace0(input)?;
    let (input, time_list) = separated_list1(multispace1,i64)(input)?;
    let (input, _) = multispace0(input)?;
    let (input, _) = tag("Distance:")(input)?;
    let (input, _) = multispace0(input)?;
    let (input, distance_list) = separated_list1(multispace1,i64)(input)?;

    let result: Vec<(i64, i64)> = std::iter::zip(time_list, distance_list).collect();
    Ok((input, result))
}

fn parse_race_list(input: &str) -> Vec<(i64,i64)> {
    let (_, race_list) = document(input).unwrap();
    race_list
}

fn solve_part_1(input: &str) -> i64 {
    let number_of_ways:Vec<i64> = parse_race_list(input).iter().map(|pair| {
        find_number_of_ways(pair.0, pair.1)
    }).collect();
    println!("{:?}", number_of_ways);
    number_of_ways.iter().product()
}

fn solve_part_2(input: &str) -> i64 {
    let number_of_ways:Vec<i64> = parse_race_list(&input.replace(" ", "")).iter().map(|pair| {
        find_number_of_ways(pair.0, pair.1)
    }).collect();
    println!("{:?}", number_of_ways);
    number_of_ways.into_iter().nth(0).unwrap()
}

#[cfg(test)]
mod tests {
    use roots::{find_roots_quadratic, Roots};
    use crate::day_6::{document, find_number_of_ways, find_whole_roots, solve_part_1, solve_part_2};
    use crate::read_file;

    #[test]
    fn should_play_with_solving_library() {
        let result = find_roots_quadratic(-1f64, 7f64, -9f64);
        assert_eq!(result, Roots::Two([1.6972243622680052, 5.302775637731995]));
    }

    #[test]
    fn should_find_whole_roots() {
        let result = find_whole_roots(7, 9);
        assert_eq!(result, (2, 5));
    }

    #[test]
    fn should_calculate_number_of_wins() {
        let result = find_number_of_ways(7, 9);
        assert_eq!(result, 4);
    }

    #[test]
    fn should_parse_sample() {
        let input = read_file("day_6/sample_part_1.txt");
        assert_eq!(document(&input), Ok(("\n", vec![(7,9), (15,40),( 30, 200)])));
    }

    #[test]
    fn should_solve_part_1_example() {
        let input = read_file("day_6/sample_part_1.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 288);
    }


    #[test]
    fn should_play_with_solving_library_error_case() {
        let result = find_roots_quadratic(-1f64, 30f64, -200f64);
        assert_eq!(result, Roots::Two([10.0, 20.0]));
    }

    #[test]
    fn should_find_whole_roots_error_case() {
        let result = find_whole_roots(30, 200);
        assert_eq!(result, (11, 19));
    }

    #[test]
    fn should_solve_part_1_real() {
        let input = read_file("day_6/input.txt");
        let result = solve_part_1(&input);
        assert_eq!(result, 303600);
    }

    #[test]
    fn should_solve_part_2_example() {
        let input = read_file("day_6/sample_part_1.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 71503);
    }

    #[test]
    fn should_solve_part_2_real() {
        let input = read_file("day_6/input.txt");
        let result = solve_part_2(&input);
        assert_eq!(result, 23654842);
    }
}