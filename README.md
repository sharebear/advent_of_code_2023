# Advent of Code 2023, in Rust

This is the repo where I publish my Rust solutions to Advent of code, and comes
with the following disclaimers.

1. I'm a Rust n00b, muddling along the best I can
2. It's AoC, I don't care about panic-ing so error handling is not great
3. I may drop off early due to time constraints.

That said, I have the vague wish to refactor into two crates, so that I can
have the implementations in no_std and have some web assembly/embedded fun with
the result.

If you follow the order of the unit tests you should be able to see some of the
thought process that went into solving the tasks, I've also tried to remember
to have separate commits for part 1 and part 2 so it's possible to see how the
solution changed between the parts.
